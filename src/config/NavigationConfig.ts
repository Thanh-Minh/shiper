import {ParamListBase} from '@react-navigation/native';

export enum ScreenMap {
    SignIn = 'SignIn',
    SignUp = 'SignUp',
    Otp = 'Otp',
    Provision = 'Provision',
    UpdateOwnerHelperInfo = 'UpdateOwnerHelperInfo',
    Order = 'Order',
    Statistics = 'Statistics',
    OrderDetail = 'OrderDetail',
    Notification = 'Notification',
    StatisticsDetail = 'StatisticsDetail',
    NotificationDetail = 'NotificationDetail',
    TakingOrder= 'TakingOrder',
    Profile='Profile',
    Account='Account',
    BankProfile='BankProfile',
    MainProfile = 'MainProfile',
    Pickup='Pickup'
}

export interface ScreenParams extends ParamListBase {
    [ScreenMap.SignIn]: {};
    [ScreenMap.SignUp]: {};

    [ScreenMap.Otp]: {};
    [ScreenMap.Provision]: {};
    [ScreenMap.UpdateOwnerHelperInfo]: {};
    [ScreenMap.Order]: {};
    [ScreenMap.TakingOrder]: {};
    [ScreenMap.OrderDetail]: { item: DetailOrder };
    [ScreenMap.Statistics]: {};
    [ScreenMap.Notification]: {};
    [ScreenMap.Profile]: {};
    [ScreenMap.StatisticsDetail]: {}
    [ScreenMap.NotificationDetail]: { item: NotifyItem }
    [ScreenMap.Account]:{};
    [ScreenMap.BankProfile]:{}
    [ScreenMap.MainProfile]: {};
    [ScreenMap.Pickup]:{}
}
