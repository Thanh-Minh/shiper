import {produce} from 'immer';
import {
    createAction,
    createActionTypeOnSuccess,
    UnfoldSagaActionType,
} from 'redux-unfold-saga';

export const REDUX_KEY = 'Info';
export enum ActionTypes {
    UPDATE_OWNER_HELPER_INFO = 'UPDATE_OWNER_HELPER_INFO',
    UPDATE_USER_BANK_INFO = 'UPDATE_USER_BANK_INFO',
    GET_USER_BANK_INFO = 'GET_USER_BANK_INFO',
    GET_PROFILE = 'GET_PROFILE',
    GET_INFO_OWNER_HELPER='GET_INFO_OWNER_HELPER',
}

export const Actions = {
    updateOwnerHelperInfo: createAction(ActionTypes.UPDATE_OWNER_HELPER_INFO),
    updateUserBankInfo: createAction(ActionTypes.UPDATE_USER_BANK_INFO),
    getUserBankInfo: createAction(ActionTypes.GET_USER_BANK_INFO),
    getProfile:createAction(ActionTypes.GET_PROFILE),
    getInfoOwnerHelper:createAction(ActionTypes.GET_INFO_OWNER_HELPER)
};

export interface InfoState {
    data_user_bank: UserBank;
    data_ownerhelper:HelperInfo
}

export const defaultState: InfoState = {
    data_user_bank: {
        accName: '',
        accNum: '',
        bankCode: '',
        bankName: '',
        createdTime: 0,
        id: 0,
        updatedTime: 0,
        userId: 0,
    },
    data_ownerhelper: {
        address: {
            buildingId: 0,
            createdTime: 0,
            districtId: 0,
            id: 0,
            latitude: 0,
            longitude: 0,
            name: '',
            updatedTime: 0,
            userId: 0,
            wardId: 0,
        },
        birthday: 0,
        fullName: '',
        gender: 0,
        phone: '',
    },
};

export const reducer = (
    state = defaultState,
    action: UnfoldSagaActionType,
): InfoState => {
    const {type, payload} = action;
    return produce(state, (draftState: InfoState) => {
        switch (type) {
            case createActionTypeOnSuccess(ActionTypes.GET_INFO_OWNER_HELPER):
                draftState.data_ownerhelper = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.UPDATE_USER_BANK_INFO):
                draftState.data_user_bank = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.GET_USER_BANK_INFO):
                draftState.data_user_bank = payload.data;
                break;
        }
    });
};
