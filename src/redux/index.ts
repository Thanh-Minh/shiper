import { combineReducers } from 'redux';

import {
  AuthState,
  defaultState as authDefaultState,
  reducer as AuthReducer,
  REDUX_KEY as AuthKey,
} from './AuthRedux';
import {
  defaultState as globalDefaultState,
  GlobalState,
  reducer as GlobalReducer,
  REDUX_KEY as GlobalKey,
} from './GlobalRedux';
import {
  defaultState as homeDefaultState,
  HomeState,
  reducer as HomeReducer,
  REDUX_KEY as HomeKey,
} from './HomeRedux';
import {
  defaultState as infoDefaultState,
  InfoState,
  reducer as InfoReducer,
  REDUX_KEY as InfoKey,
} from './InfoRedux';

export interface RootState {
  [HomeKey]: HomeState;
  [GlobalKey]: GlobalState;
  [AuthKey]: AuthState;
  [InfoKey]: InfoState;
}
export const RootStateDefault: RootState = {
  [HomeKey]: homeDefaultState,
  [GlobalKey]: globalDefaultState,
  [AuthKey]: authDefaultState,
  [InfoKey]: infoDefaultState,
};

export default combineReducers<RootState>({
  [HomeKey]: HomeReducer,
  [GlobalKey]: GlobalReducer,
  [AuthKey]: AuthReducer,
  [InfoKey]: InfoReducer,
});
