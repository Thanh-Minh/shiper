import {produce} from 'immer';
import {
    createAction,
    createActionTypeOnSuccess,
    UnfoldSagaActionType,
} from 'redux-unfold-saga';

export const REDUX_KEY = 'Auth';
export enum ActionTypes {
    LOGIN = 'LOGIN',
    SEND_REGISTER_DATA = 'SEND_REGISTER_DATA',
    VERIFY_PHONE = 'VERIFY_PHONE',
    ACCEPT_TERM_OF_SERVICE = 'ACCEPT_TERM_OF_SERVICE',
    RESET_AUTH = 'RESET_AUTH',
    SEND_OTP = 'SEND_OTP',
    INSERT_DEVICE_FIRE_BASE = 'INSERT_DEVICE_FIRE_BASE',
}

export const Actions = {
    login: createAction(ActionTypes.LOGIN),
    sendRegisterData: createAction(ActionTypes.SEND_REGISTER_DATA),
    verifyPhone: createAction(ActionTypes.VERIFY_PHONE),
    acceptTermOfService: createAction(ActionTypes.ACCEPT_TERM_OF_SERVICE),
    resetAuth: createAction(ActionTypes.RESET_AUTH),
    sendOtp: createAction(ActionTypes.SEND_OTP),
    insertDeviceFireBase: createAction(ActionTypes.INSERT_DEVICE_FIRE_BASE),
};

export interface AuthState {
    data_auth: AuthInfo;
    user_info: UserInfo;
}

export const defaultState: AuthState = {
    data_auth: {
        access_token: '',
        access_token_refresh: '',
    },
    user_info: {
        default_address: '',
        expiredTime: 0,
        fullName: '',
        source: '',
        status: 0,
        token: '',
        userId: 0,
        userName: '',
    },
};

export const reducer = (
    state = defaultState,
    action: UnfoldSagaActionType,
): AuthState => {
    const {type, payload} = action;
    return produce(state, (draftState: AuthState) => {
        switch (type) {
            case createActionTypeOnSuccess(ActionTypes.LOGIN):
                if(payload.data){
                    draftState.data_auth = {
                        access_token: payload.data.access_token,
                        access_token_refresh: payload.data.access_token_refresh,
                    };
                    draftState.user_info = payload.data.user;
                }
                break;
            case createActionTypeOnSuccess(ActionTypes.SEND_REGISTER_DATA):
                if(payload.data){
                    draftState.data_auth = {
                        access_token: payload.data.access_token,
                        access_token_refresh: payload.data.access_token_refresh,
                    };
                    draftState.user_info = payload.data.user;
                }
                break;
            case createActionTypeOnSuccess(ActionTypes.RESET_AUTH):
                draftState = defaultState;
                break;


        }
    });
};
