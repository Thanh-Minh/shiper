import { produce } from 'immer';
import {
    createAction,
    createActionTypeOnSuccess,
    UnfoldSagaActionType
} from 'redux-unfold-saga';

export const REDUX_KEY = 'Data';
export enum ActionTypes {
    GET_DISTRICT = 'GET_DISTRICT',
    GET_WARD = 'GET_WARD',
    GET_BUILDING = 'GET_BUILDING',
    UPLOAD_IMAGE = 'UPLOAD_IMAGE',
    GET_LIST_NEW_ORDER = 'GET_LIST_NEW_ORDER',
    COMPLETE_ORDER = 'COMPLETE_ORDER',
    PREPARE_ORDER = 'PREPARE_ORDER',
    DETAIL_ORDER= 'DETAIL_ORDER',
    PUT_CARTS= 'PUT_CARTS',
    GET_DATA_STATISTIC = 'GET_DATA_STATISTIC',
    GET_LIST_BANK = 'GET_LIST_BANK',
    PUT_CARTS_SUCCESS='PUT_CARTS_SUCCESS',
    GET_PICKUP='GET_PICKUP',

}

export const Actions = {
    getDistrict: createAction(ActionTypes.GET_DISTRICT),
    getWard: createAction(ActionTypes.GET_WARD),
    getBuilding: createAction(ActionTypes.GET_BUILDING),
    uploadImage: createAction(ActionTypes.UPLOAD_IMAGE),
    getListOrder: createAction(ActionTypes.GET_LIST_NEW_ORDER),
    completeOrder: createAction(ActionTypes.COMPLETE_ORDER),
    prepareOrder:createAction(ActionTypes.PREPARE_ORDER),
    detailOrder:createAction(ActionTypes.DETAIL_ORDER),
    putcarts: createAction(ActionTypes.PUT_CARTS),
    getDataStatistic: createAction(ActionTypes.GET_DATA_STATISTIC),
    getListBank: createAction(ActionTypes.GET_LIST_BANK),
    putcartssuccess:createAction(ActionTypes.PUT_CARTS_SUCCESS),
    getPickup:createAction(ActionTypes.GET_PICKUP),



};

export interface DataState {
    data_district: {
        createdTime: string;
        id: number;
        name: string;
        updatedTime: string;
    }[];
    data_ward: {
        createdTime: number;
        districtId: number;
        id: number;
        name: string;
        updatedTime: number;
    }[];
    data_building: {
        createdTime: number,
        districtId: number,
        id: number,
        latitude: number,
        name: string,
        longitude: number,
        updatedTime: number,
        wardId:number,

    }[];
    data_list_order: NewOrderItem[];
    data_list_orders:OrdersItem[];
    data_pickup:Pickup[];
}


export const defaultState: DataState = {
    data_district: [
        {
            createdTime: '',
            id: 0,
            name: '',
            updatedTime: '',
        },
    ],
    data_ward: [
        {
            createdTime: 0,
            districtId: 0,
            id: 0,
            name: '',
            updatedTime: 0,
        },
    ],
    data_building: [
        {
            createdTime: 0,
            districtId: 0,
            id: 0,
            latitude: 0,
            name: '',
            longitude: 0,
            updatedTime: 0,
            wardId:0
        },
    ],
    data_list_order:[
        {
            callBackUrl: '',
            code: '',
            createdTime: 0,
            deliveryAddress: '',
            deliveryNote: '',
            deliveryTime: 0,
            description: '',
            feeShip: 0,
            helperId: 0,
            id: 0,
            paymentId: 0,
            paymentType: 0,
            paymentValue: 0,
            status: 0,
            successTime: 0,
            totalProduct: 0,
            totalShop: 0,
            updatedTime: 0,
            userId: 0
        }
    ],
    data_list_orders:[
        {
            callBackUrl: '',
            code: '',
            createdTime: 0,
            deliveryAddress: '',
            deliveryNote: '',
            deliveryTime: 0,
            description: '',
            feeShip: 0,
            helperId: 0,
            id: 0,
            paymentId: 0,
            paymentType: 0,
            paymentValue: 0,
            status: 0,
            successTime: 0,
            totalProduct: 0,
            totalShop: 0,
            updatedTime: 0,
            userId: 0
        }
    ],
    data_pickup:[
        {
            confirmImage: '',
            id: 0,
            note: '',
            orderCode: '',
            packingType: '',
            productName: '',
            quantity: 0,
            shopAddress: '',
            shopId: 0,
            shopName: '',
            shopPhoneNumber: '',
            status: 0,
        }
    ]


};

export const reducer = (
    state = defaultState,
    action: UnfoldSagaActionType,
): DataState => {
    const {type, payload} = action;
    return produce(state, (draftState: DataState) => {
        switch (type) {
            case createActionTypeOnSuccess(ActionTypes.GET_DISTRICT):
                draftState.data_district = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.GET_WARD):
                draftState.data_ward = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.GET_BUILDING):
                draftState.data_building = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.GET_LIST_NEW_ORDER):
                draftState.data_list_order = payload.data;
                break;
            case createActionTypeOnSuccess(ActionTypes.PREPARE_ORDER):
                draftState.data_list_orders = payload.data
                break
            case createActionTypeOnSuccess(ActionTypes.GET_PICKUP):
                draftState.data_pickup = payload.data
                break
        }
    });
};
