import configureStore from './lib/configureStore'

const store = configureStore();
export default store;
