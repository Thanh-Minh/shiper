import {RootState} from "../redux";
import {DataState, REDUX_KEY} from "../redux/DataRedux";
import {get} from 'lodash';
import {createSelector} from "reselect";

export const selectData = (state: RootState): DataState =>
    get(state, REDUX_KEY);
