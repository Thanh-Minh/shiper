import { get } from 'lodash';
import { createSelector } from 'reselect';
import { RootState } from '../redux';
import { GlobalState, REDUX_KEY } from '../redux/GlobalRedux';

export const selectGlobal = (state: RootState): GlobalState => get(state, REDUX_KEY);

export const selectGlobalLoading = createSelector(
  selectGlobal,
  (gloabalState: GlobalState): boolean => gloabalState.isLoading,
);

export default {
  selectGlobal,
};
