import media from '../../assets/media';
import React, {ReactElement} from 'react';
import {Image} from 'react-native';

const ImageHeader = (): ReactElement => {
  return (
    <Image
      source={media.header_background}
      style={{resizeMode: 'cover', height: '100%', width: '100%'}}
    />
  );
};
export default ImageHeader;
