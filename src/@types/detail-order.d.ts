interface DetailOrder{
    callBackUrl: string,
    cartForShipperDtos: [
        {
            confirmImage: string,
            id: number,
            note: string,
            orderCode: string,
            packingType: string,
            productName: string,
            quantity: number,
            shopAddress: string,
            shopId: number,
            shopName: string,
            shopPhoneNumber: string,
            status: number
        }
    ],
    code: string,
    createdTime: number,
    deliveryAddress: string,
    deliveryNote: string,
    deliveryTime: number,
    description: string,
    feeShip: number,
    helperId: number,
    id: number,
    paymentId: number,
    paymentType: number,
    paymentValue: number,
    shopForShipperDtos: [
        { shopAddress: string,
            shopName: string }
    ],
    status: number,
    successTime: number,
    totalProduct: number,
    totalShop: number,
    updatedTime: number,
    deliveryPhoneNumber:string,
    confirmImage: string,
    userId: number
}
interface cartForShipperDtos{
    confirmImage: string,
    id: number,
    note: string,
    orderCode: string,
    packingType: string,
    productName: string,
    quantity: number,
    shopAddress: string,
    shopId: number,
    shopName: string,
    shopPhoneNumber: string,
    status: number,

}
// interface shopForShipperDtos{
//
//     shopAddress: string,
//     shopName: string
//
// }
