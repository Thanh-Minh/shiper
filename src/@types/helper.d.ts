interface UpdateHelperInfo {
    address: string;
    birthday: number;
    building_id:number;
    full_name: string;
    gender: number;
    ward_id: number;
    cert_images:string[];
}
interface HelperInfo {
    address: {
        buildingId: number;
        createdTime: number;
        districtId: number;
        id: number;
        latitude: number;
        longitude: number;
        name: string;
        updatedTime: number;
        userId: number;
        wardId: number;
    };
    birthday: number;
    fullName: string;
    gender: number;
    phone: string;
}
interface UserBank {
    accName: string;
    accNum: string;
    bankCode: string;
    bankName: string;
    createdTime: number;
    id: number;
    updatedTime: number;
    userId: number;
}
interface UserBank {
    accName: string;
    accNum: string;
    bankCode: string;
    bankName: string;
    createdTime: number;
    id: number;
    updatedTime: number;
    userId: number;
}
