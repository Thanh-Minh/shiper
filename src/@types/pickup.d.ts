interface Pickup{
    confirmImage: string,
    id: number,
    note: string,
    orderCode: string,
    packingType: string,
    productName: string,
    quantity: number,
    shopAddress: string,
    shopId: number,
    shopName: string,
    shopPhoneNumber: string,
    status: number,
}
