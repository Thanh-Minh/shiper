export const convert_dropdown_data = (data: []) => {
    let new_data: {
        label: string;
        value: number;
    }[] = [];
    data.map((item: any) => {
        new_data.push({
            label: item.name,
            value: item.id,
        });
    });
    return new_data
};
export const convert_dropdown_bank_data = (data: []) => {
    let new_data: {
        label: string;
        value: number;
    }[] = [];
    data.map((item: any) => {
        new_data.push({
            label: item.name,
            value: item.code,
        });
    });
    return new_data;
};
export const numberWithDots = (number: number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

