export const validate_phone = (txt: string) => {
    const reg = /^[0-9\+]{9,11}$/g;
    return reg.test(txt);
};

export const validate_password = (txt: string) => {
    const reg = /^[a-zA-Z0-9\+]{6,12}$/g;
    return reg.test(txt);
};

const removeAscent = (txt: string) => {
    if (txt === null || txt === undefined) return txt;
    txt = txt.toLowerCase();
    txt = txt.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    txt = txt.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    txt = txt.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    txt = txt.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    txt = txt.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    txt = txt.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    txt = txt.replace(/đ/g, 'd');
    return txt;
};
export const validate_name = (txt: string) => {
    const reg = /^[a-zA-Z_ ]*$/g;
    return reg.test(removeAscent(txt));
};

export const validate_address = (txt: string) => {
    const reg = /^[a-zA-Z0-9_ ]*$/g;
    return reg.test(removeAscent(txt));
};

