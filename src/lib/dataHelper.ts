import {Platform} from 'react-native';

export const createFormData = (photo: any) => {
  const data = new FormData();
  const fileName =
      Platform.OS === 'ios'
          ? photo.path.split('/')[photo.path.split('/').length - 1]
          : photo.path.replace('file://', '').split('/')[
          photo.path.replace('file://', '').split('/').length - 1
              ];
  data.append('files', {
    name: fileName,
    type: photo.mime,
    uri:
        Platform.OS === 'android'
            ? photo.path
            : photo.path.replace('file://', ''),
    data: photo.data,
  });
  return data;
};
