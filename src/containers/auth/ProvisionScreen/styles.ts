import {screenHeight} from '../../../config/ScreenConfig';
import {Palette} from '../../../theme/Palette';
import {Platform, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
  },
  scroll: {
    marginTop: Platform.OS === 'android' ? 20 : 0,
    marginBottom: 30,
  },
  groupContent: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 15,
  },
  title: {
    color: '#FF3B10',
    fontSize: RFValue(20, 580),
    fontWeight: 'bold',
    lineHeight: 26,
  },
  subTitle: {
    marginVertical: 10,
    fontSize: RFValue(10, 580),
    color: Palette.color_1f1,
    lineHeight: 14,
  },
  image: {
    height: screenHeight / 4.5,
    resizeMode: 'contain',
  },
  thank: {
    marginTop: 10,
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 14,
    textAlign: 'center',
  },
  provisonTitle: {
    marginTop: 20,
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  provisonSection: {
    fontSize: RFValue(8.5, 580),
    fontWeight: 'bold',
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  provisonSectionContent: {
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    alignSelf: 'flex-start',
  },
  groupInput: {
    height: 50,
    width: '100%',
    alignSelf: 'flex-start',
    marginTop: 20,
    marginBottom: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  labelInput: {
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
  },
  input: {
    flex: 1,
    fontSize: RFValue(8.5, 580),
    color: Palette.color_1f1,
  },
  groupImage: {
    flex: 1,
    width: '100%',
    alignSelf: 'flex-start',
    marginVertical: 10,
  },
  titleImage: {
    fontSize: RFValue(8, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
  },
  descriptionImage: {
    fontSize: RFValue(8, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    fontStyle: 'italic',
    fontWeight: '300',
  },
  groupImagePicker: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-around',
  },
  groupConfirm: {
    height: 50,
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 10,
  },
  iconCheck: {
    width: 18,
    height: 18,
  },
  textConfirm: {
    fontSize: RFValue(9.5, 580),
    marginLeft: 10,
  },
});
