import {Palette} from '../../../theme/Palette';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    width:'100%',
    backgroundColor: Palette.white,


  },
  groupContent: {
    marginBottom: 40,
    marginTop:'50%'


  },
  groupSubmit: {
    marginTop: 10,
  },
  groupTextBelow: {
    height: 30,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
  },
  hairline: {
    flex: 1,
    height: 0.5,
    backgroundColor: Palette.color_ccc,
  },
  loginButtonBelowText1: {
    width: 130,
    textAlign: 'center',
    color: Palette.color_ccc,
  },
  validate: {
    color: 'red',
    marginTop: 9,
    marginHorizontal: 20,
  },
});
