import {ScreenMap} from "../../../config/NavigationConfig";
import {Palette} from "../../../theme/Palette";
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import styles from "./styles";
import {View, Text, SafeAreaView} from "react-native";
import {BaseScreenProps} from "../../../@types/screen-type";
import React,{ReactElement, useCallback, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {selectAuthData, selectUserData} from "../../../selector/AuthSelector";
import {validate_password, validate_phone} from "../../../lib/validateHelpers";
import {Actions as AuthActions} from '../../../redux/AuthRedux';
import {Actions as GlobalAction} from '../../../redux/GlobalRedux';
import {code, user_code} from "../../../lib/codeHelpers";
import {updateAccessTokenToHeader} from "../../../lib/fetchHelpers";
import AsyncStorage from "@react-native-community/async-storage";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Input from "../../../components/Form/Input";
import media from "../../../assets/media";

const SignInScreen = (
    props: BaseScreenProps<ScreenMap.SignIn>,
): ReactElement => {
  const dispatch = useDispatch();
  const dataAuth = useSelector(selectAuthData);
  const dataUser = useSelector(selectUserData);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [validatePhone, setValidatePhone] = useState('');
  const [validatePassword, setValidatePassword] = useState('');



  useEffect(() => {
    const unsubcribe = props.navigation.addListener('focus', () => {
      setPhone('');
      setPassword('');
      setValidatePhone('');
      setValidatePassword('');
    });
    return unsubcribe;
  }, [props.navigation]);
  const validate_data = (phone: string, password: string) => {
    let validate_fail = 0;
    if (phone.length <= 0) {
      setValidatePhone('Vui lòng nhập số điện thoại');
      validate_fail += 1;
    } else if (!validate_phone(phone)) {
      setValidatePhone('Vui lòng nhập số điện thoại từ 9 đến 11 số');
      validate_fail += 1;
    }
    if (password.length <= 0) {
      setValidatePassword('Vui lòng nhập mật khẩu');
      validate_fail += 1;
    } else if (!validate_password(password)) {
      setValidatePassword(
          'Vui lòng nhập mật khẩu tối thiểu 6 ký , gồm số hoặc chữ',
      );
      validate_fail += 1;
    }
    return validate_fail <= 0;
  };
  const login = useCallback(
      (phone: string, password: string) => {
        if (validate_data(phone, password)) {
          dispatch(
              AuthActions.login(
                  {
                    phone: phone,
                    password: password,
                  },
                  {
                    onBeginning: () => {
                      dispatch(GlobalAction.setShowLoading({isLoading: true}));
                    },
                    onSuccess: (response: any) => {
                      let direct = [
                        ScreenMap.Otp,
                        ScreenMap.Provision,
                        ScreenMap.UpdateOwnerHelperInfo,
                      ];
                      switch (response.errorCode) {
                        case code.success:
                          switch (response.data.user.status) {
                            case user_code.block:
                              setValidatePassword('Tài khoản của bạn đã bị khoá');
                              break;
                            case user_code.accepted_tos:
                              updateAccessTokenToHeader({
                                accessToken: response.data.access_token,
                              });

                              props.navigation.navigate(ScreenMap.UpdateOwnerHelperInfo, {});
                              setPhone('')
                              setPassword('')
                              break;
                            case user_code.updated_info:
                              updateAccessTokenToHeader({
                                accessToken: response.data.access_token,
                              });
                              AsyncStorage.setItem(
                                  'access_token',
                                  response.data.access_token,
                              );
                              props.navigation.navigate('BottomTabStack', {
                                screen: 'Order',
                              });
                              setPhone('')
                              setPassword('')
                              break;
                            case user_code.register_done:
                              updateAccessTokenToHeader({
                                accessToken: response.data.access_token,
                              });
                              props.navigation.navigate('BottomTabStack', {
                                screen: 'Order',
                              });
                              setPhone('')
                              setPassword('')
                              break;
                            default:
                              updateAccessTokenToHeader({
                                accessToken: response.data.access_token,
                              });
                              AsyncStorage.setItem(
                                  'access_token',
                                  response.data.access_token,
                              );
                              props.navigation.navigate(
                                  direct[response.data.user.status],
                                  {},
                              );
                              setPhone('')
                              setPassword('')
                              break;

                          }
                          break;
                        default:
                          setValidatePassword(response.message);
                          break;
                      }
                    },
                    onFailure: (error: any) => {
                      console.warn('error', error);
                    },
                    onFinish: () => {
                      dispatch(GlobalAction.setShowLoading({isLoading: false}));
                    },
                  },
              ),
          );
        }
      },
      [dispatch],
  );
  const _Signup=()=>{
    setPhone(''),
        setPassword(''),
        props.navigation.navigate(ScreenMap.SignUp, {})
  }

  return (
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView>
          <View style={styles.groupContent}>
            <Input
                label="Số điện thoại"
                image={media.phone}
                maxLength={11}
                value={phone}
                keyboardNumber={true}
                placeHolder="Nhập số điện thoại của bạn"
                placeHolderColor={Palette.color_ccc}
                setValue={setPhone}
                removeValidateText={setValidatePhone}
            />
            {validatePhone.length > 0 ? (
                <Text style={styles.validate}>{validatePhone}</Text>
            ) : null}
            <Input
                label="Mật khẩu"
                image={media.lock}
                maxLength={12}
                value={password}
                placeHolder="*******"
                placeHolderColor={Palette.color_ccc}
                isPassword={true}
                setValue={setPassword}
                removeValidateText={setValidatePassword}
            />
            {validatePassword.length > 0 ? (
                <Text style={styles.validate}>{validatePassword}</Text>
            ) : null}
            <View style={styles.groupSubmit}>
              <ButtonSubmit
                  title="Đăng Nhập"
                  color={[Palette.color_ff0, Palette.color_ff4]}
                  action={() => login(phone, password)}
              />
              <ButtonSubmit
                  title="Đăng Ký"
                  color={[Palette.color_f48, Palette.color_f48]}
                  action={_Signup}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
  );
};
export default SignInScreen;
