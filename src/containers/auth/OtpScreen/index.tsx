import {BaseScreenProps} from "../../../@types/screen-type";
import React, {ReactElement, useCallback, useState} from "react";
import {ScreenMap} from "../../../config/NavigationConfig";
import {useDispatch} from "react-redux";
import {code} from "../../../lib/codeHelpers";
import {Actions as AuthActions} from '../../../redux/AuthRedux';
import {ActivityIndicator, Alert, SafeAreaView, View,Text} from "react-native";
import styles from "./styles";
import {Palette} from "../../../theme/Palette";
import OTPInputView from "../../../Module/@twotalltotems/react-native-otp-input/dist";
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import TextLink from "../../../components/Form/TextLink";
import {RFValue} from "react-native-responsive-fontsize";
import OtpInputs from "react-native-otp-inputs";
const OtpScreen = (props: BaseScreenProps<ScreenMap.Otp>): ReactElement => {
    const dispatch = useDispatch();
    const [otp, setOtp] = useState('');
    const [loading, setLoading] = useState(false);
    const sendOtp = useCallback(() => {
        dispatch(
            AuthActions.sendOtp(
                {
                    typeSend: 0,
                },
                {
                    onBeginning: () => {},
                    onSuccess: (response: any) => {
                        switch (response.errorCode) {
                            case code.unauthorize:
                                props.navigation.navigate(ScreenMap.SignIn, {});
                                break;
                        }
                    },
                    onFailure: (error: any) => {
                        console.warn('error', error);
                    },
                    onFinish: () => {},
                },
            ),
        );
    }, []);

    const verifyPhone = useCallback(
        (otp: string) => {
            if (otp.length == 4) {
                dispatch(
                    AuthActions.verifyPhone(
                        {
                            otp: otp,
                        },
                        {
                            onBeginning: () => {
                                setLoading(true);
                            },
                            onSuccess: (response: any) => {
                                switch (response.errorCode) {
                                    case code.success:
                                        props.navigation.navigate(ScreenMap.Provision, {});
                                        break;
                                    case code.otp_code_wrong:
                                        Alert.alert('Mã OTP sai, vui lòng thử lại');
                                        break;
                                    case code.unauthorize:
                                        props.navigation.navigate(ScreenMap.SignIn, {});
                                        break;
                                }
                            },
                            onFailure: (error: any) => {
                                console.warn('error', error);
                            },
                            onFinish: () => {
                                setLoading(false);
                            },
                        },
                    ),
                );
            }
            else {Alert.alert('Mã OTP sai, vui lòng thử lại')}
        },
        [dispatch],
    );

    return (
        <SafeAreaView style={styles.container}>
            {loading ? (
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        backgroundColor: Palette.black,
                        opacity: 0.7,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <ActivityIndicator
                        animating={true}
                        size="large"
                        color={Palette.white}
                    />
                </View>
            ) : null}
            <View style={styles.otp}>
                <Text style={styles.otpTitle}>
                    {'Nhập mã OTP vừa được gửi vào số điện thoại của bạn.'}
                </Text>
                <OTPInputView
                    pinCount={4}
                    style={styles.otpInput}
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    placeholderCharacter="-"
                    placeholderTextColor={Palette.color_1e2}
                    autoFocusOnLoad={false}
                    onCodeFilled={code => {
                        setOtp(code);
                    }}
                />

                <ButtonSubmit
                    title="Xong"
                    color={[Palette.color_ff0, Palette.color_ff4]}
                    action={() => verifyPhone(otp)}
                />
                <TextLink
                    textTitle="Bạn không nhận được mã OTP?"
                    textLink="Gửi lại"
                    fontSize={RFValue(12, 580)}
                    color={Palette.color_e40}
                    action={() => {
                        sendOtp();
                    }}
                />
            </View>
        </SafeAreaView>
    );
};
export default OtpScreen;
