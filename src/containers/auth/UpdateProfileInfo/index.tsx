import {BaseScreenProps} from "../../../@types/screen-type";
import {Actions as DataAction} from '../../../redux/DataRedux';
import {Actions as GlobalAction} from '../../../redux/GlobalRedux';
import {Actions as InfoAction} from '../../../redux/InfoRedux';
import moment from 'moment';
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {Alert, Image, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {RFValue} from 'react-native-responsive-fontsize';
import {useDispatch} from 'react-redux';
import styles from './styles';
import media from "../../../assets/media";
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import Input from "../../../components/Form/Input";
import ImageHolder from "../../../components/ImageHolder";
import {ScreenMap} from "../../../config/NavigationConfig";
import {code} from "../../../lib/codeHelpers";
import {convert_dropdown_data} from "../../../lib/convertDataHelper";
import {validate_address, validate_name} from "../../../lib/validateHelpers";
import {Palette} from "../../../theme/Palette";
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {updateAccessTokenToHeader} from "../../../lib/fetchHelpers";



const UpdateOwnerHeaderInfoScreen = (
    props: BaseScreenProps<ScreenMap.UpdateOwnerHelperInfo>,
): ReactElement => {
    const gender_type = {man: 1, woman: 0};
    const dispatch = useDispatch();
    const [date, setDate] = useState('Vui lòng chọn Ngày sinh');
    const [gender, setGender] = useState(1);
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [listImage, setListImage] = useState(['','','']);
    const [listDistrict, setListDistrict] = useState([
        {
            label: 'Dữ liệu hiện tại chưa có',
            value: -1,
        },
    ]);
    const [building, setBuilding] = useState(-1);
    const [listBuilding, setListBuilding] = useState([
        {
            label: 'Vui lòng chọn Quận/Huyện để lấy thông tin',
            value: -1,
        },
    ]);
    const [ward, setWard] = useState(-1);
    const [listWard, setListWard] = useState([
        {
            label: 'Vui lòng chọn Quận/Huyện để lấy thông tin',
            value: -1,
        },
    ]);
    const [districtVisible, setDistrictVisible] = useState(false);
    const [wardVisible, setWardVisible] = useState(false);
    const [validateName, setValidateName] = useState('');
    const [validateDate, setValidateDate] = useState('');
    const [validateWard, setValidateWard] = useState('');
    const [validateAddress, setValidateAddress] = useState('');
    const [validateListImage, setValidateListImage] = useState('');
    const [validateBuilding, setValidateBuilding] = useState('');
    const [showDatePicker, setShowDatePicker] = useState(false);
    useEffect(() => {
        const unsubcribe = props.navigation.addListener('focus', () => {
            dispatch(
                DataAction.getDistrict(
                    {},
                    {
                        onBeginning: () => {},
                        onSuccess: (data: any) => {
                            setListDistrict(convert_dropdown_data(data.data));
                        },
                        onFailure: () => {},
                        onFinish: () => {},
                    },
                ),
            );
        });
        return unsubcribe;
    }, [dispatch, props.navigation]);

    const getListWard = useCallback(
        (district_id: number) => {
            dispatch(
                DataAction.getWard(
                    {district_id: district_id},
                    {
                        onBeginning: () => {},
                        onSuccess: (data: any) => {
                            setWard(-1);
                            setListWard(convert_dropdown_data(data.data));
                        },
                        onFailure: () => {},
                        onFinish: () => {},
                    },
                ),
            );
        },
        [dispatch],
    );
    const getListBuilding = useCallback(
        (id: number) => {
            dispatch(
                DataAction.getBuilding(
                    {id: id},
                    {
                        onBeginning: () => {},
                        onSuccess: (data: any) => {
                            setBuilding(-1)
                            setListBuilding(convert_dropdown_data(data.data));
                        },
                        onFailure: () => {},
                        onFinish: () => {},
                    },
                ),
            );
        },
        [dispatch],
    );

    const validate_data = (
        address: string,
        date: string,
        name: string,
        ward: number,
        building:number,
        listImage: string[],
    ) => {
        let validate_fail = 0;
        if (name.length <= 0) {
            setValidateName('Vui lòng nhập Họ và tên');
            validate_fail += 1;
        } else if (!validate_name(name)) {
            setValidateName('Vui lòng nhập Họ và tên không bao gồm ký tự đặc biệt');
            validate_fail += 1;
        }else {
            setValidateName('');
        }
        if (date.length <= 0) {
            setValidateDate('Vui lòng chọn Ngày sinh');
            validate_fail += 1;
        } else {
            setValidateDate('');
        }
        if (ward < 0) {
            setValidateWard('Vui lòng chọn Phường/Xã');
            validate_fail += 1;
        } else {
            setValidateWard('');
        }
        if (building < 0) {
            setValidateBuilding('Vui lòng chọn toà nhà bạn nhận ship');
            validate_fail += 1;
        }
        else {
            setValidateWard('');
        }
        if (address.length <= 0) {
            setValidateAddress('Vui lòng nhập Địa chỉ');
            validate_fail += 1;
        }
         else if (!validate_address(address)) {
            setValidateAddress('Vui lòng nhập Địa chỉ không bao gồm ký tự đặc biệt');
            validate_fail += 1;
        }
        else {
            setValidateAddress('');
        }
        if  (listImage.filter(Boolean).length < 3)  {
            setValidateListImage(
                'Vui lòng thêm đủ ảnh giấy tờ tuỳ thân để xác minh danh tính',
            );
            validate_fail += 1;
        }else {
            setValidateListImage('');
        }
        return validate_fail == 0;
    };

    const updateOwnerInfo = useCallback(
        (
            address: string,
            date: string,
            name: string,
            gender: number,
            ward: number,
            building:number,
            listImage: string[],
        ) => {
            if (validate_data(address, date, name, ward,building ,listImage)) {
                dispatch(
                    InfoAction.updateOwnerHelperInfo(
                        {
                            address: address,
                            birthday: new Date(
                                moment(date, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                            ).getTime(),
                            building_id:building,
                            listImage: listImage,
                            full_name: name,
                            gender: gender,
                            ward_id: ward,
                        },
                        {
                            onBeginning: () => {
                                dispatch(GlobalAction.setShowLoading({isLoading: true}));
                            },
                            onSuccess: (response: any) => {
                                switch (response.errorCode){
                                    case code.success:
                                        updateAccessTokenToHeader({
                                            accessToken: response.data.access_token,
                                        });
                                        props.navigation.navigate('BottomTabStack', {
                                            screen: 'Order',
                                        });
                                        break
                                    case code.unauthorize:
                                        props.navigation.navigate(ScreenMap.SignIn, {});
                                        break;
                                }
                            },
                            onFailure: () => {},
                            onFinish: () => {
                                dispatch(GlobalAction.setShowLoading({isLoading: false}));
                            },
                        },
                    ),
                );
            }
        },
        [],
    );

    const renderDatePicker = () => {
        return (
            <View style={styles.containerDate}>
                <Text style={styles.label}>
                    {'Ngày sinh'}
                    <Text style={styles.required}>{' *'}</Text>
                </Text>
                <View style={styles.groupIconDate}>
                    <Image style={styles.icon} source={media.calendar} />
                    <TouchableOpacity
                        style={{flex: 1, alignSelf: 'center'}}
                        onPress={() => {
                            setShowDatePicker(true);
                        }}>
                        <Text
                            style={{
                                fontSize: RFValue(10, 580),
                                color: date.includes('-') ? Palette.black : Palette.color_ccc,
                            }}>
                            {date}
                        </Text>

                    </TouchableOpacity>
                    <DateTimePickerModal
                        isVisible={showDatePicker}
                        mode="date"
                        is24Hour={true}
                        minimumDate={
                            new Date(new Date().setFullYear(new Date().getFullYear() - 90))
                        }
                        maximumDate={
                            new Date(new Date().setFullYear(new Date().getFullYear() - 15))
                        }
                        display={'spinner'}
                        date={new Date()}
                        onConfirm={(date: any) => {
                            setShowDatePicker(false);
                            setDate(moment(date).format('DD-MM-YYYY'));
                        }}
                        onCancel={() => {
                            setShowDatePicker(false);
                        }}
                    />
                </View>
            </View>
        );
    };

    const renderRadioGroup = () => {
        return (
            <View style={styles.containerDate}>
                <Text style={styles.label}>{'Giới tính'}</Text>
                <View style={styles.groupRadio}>
                    <View style={styles.groupIconRadio}>
                        <TouchableOpacity onPress={() => setGender(gender_type.man)}>
                            <Image
                                source={
                                    gender == gender_type.man
                                        ? media.radio_checked
                                        : media.radio_unchecked
                                }
                                style={styles.iconRadio}
                            />
                        </TouchableOpacity>
                        <Text>{'Nam'}</Text>
                    </View>
                    <View style={styles.groupIconRadio}>
                        <TouchableOpacity onPress={() => setGender(gender_type.woman)}>
                            <Image
                                source={
                                    gender == gender_type.woman
                                        ? media.radio_checked
                                        : media.radio_unchecked
                                }
                                style={styles.iconRadio}
                            />
                        </TouchableOpacity>
                        <Text>{'Nữ'}</Text>
                    </View>
                </View>
            </View>
        );
    };
    const renderDropdown = (
        label: string,
        data: { label: string; value: number }[],
        placeHolder: string,
        zIndex: number,
    ) => {
        return (
            <>
                <Text
                    style={[
                        styles.label,
                        {flex: 1, marginHorizontal: 20, marginTop: 10},
                    ]}>
                    {label}
                    <Text style={styles.required}>{' *'}</Text>
                </Text>
                <DropDownPicker
                    zIndex={zIndex}
                    items={data}
                    placeholder={placeHolder}
                    searchable={true}
                    defaultValue={zIndex == 4000 ? (ward == -1 ? null : ward) : null}
                    searchablePlaceholder="Tìm kiếm"
                    searchablePlaceholderTextColor="gray"
                    searchableError={() => <Text>{'Không tìm thấy'}</Text>}
                    containerStyle={{height: 40, marginHorizontal: 20, marginTop: 10}}
                    style={{
                        backgroundColor: Palette.white,
                        borderWidth: 0,
                        borderBottomWidth: 0.5,
                        borderBottomColor: Palette.color_ccc,
                    }}
                    itemStyle={{
                        justifyContent: 'flex-start',
                        borderBottomWidth: 0.5,
                        borderBottomColor: Palette.color_ccc,
                    }}
                    dropDownStyle={{backgroundColor: Palette.white}}
                    placeholderStyle={{
                        color: Palette.color_ccc,
                        fontSize: RFValue(10, 580),
                    }}
                    selectedLabelStyle={{color: Palette.black}}
                    activeLabelStyle={{color: Palette.color_ff4}}
                    isVisible={zIndex == 5000 ? districtVisible : wardVisible }
                    onOpen={() => {
                        switch (zIndex) {
                            case 5000:

                                setWardVisible(false), setDistrictVisible(true);
                                break;
                            case 4000:
                                setDistrictVisible(false), setWardVisible(true);
                                break


                        }
                    }}
                    onClose={() => {
                        zIndex == 5000 ? setDistrictVisible(false) : setWardVisible(false) ;
                    }}
                    onChangeItem={item => {
                        switch (zIndex) {
                            case 5000:
                                getListBuilding(item.value)
                                getListWard(item.value);
                                break;
                            case 4000:
                                setWard(item.value);
                                break;

                        }
                    }}
                />
            </>
        );
    };
    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
                <View style={styles.form}>
                    <Input
                        label="Họ và tên"
                        image={media.user}
                        placeHolder="Nhập Họ và tên của bạn"
                        placeHolderColor={Palette.color_ccc}
                        isRequired={true}
                        value={name}
                        setValue={setName}
                        maxLength={40}
                    />
                    {validateName.length > 0 ? (
                        <Text style={styles.validate}>{validateName}</Text>
                    ) : null}
                    {renderDatePicker()}
                    {validateDate.length > 0 ? (
                        <Text style={styles.validate}>{validateDate}</Text>
                    ) : null}
                    {renderRadioGroup()}
                    {renderDropdown(
                        'Quận/Huyện',
                        listDistrict,
                        'Vui lòng chọn Quận/ Huyện',
                        5000,
                    )}
                    {renderDropdown(
                        'Phường/Xã',
                        listWard,
                        'Vui lòng chọn Phường/ Xã',
                        4000,
                    )}
                    {validateWard.length > 0 ? (
                        <Text style={styles.validate}>{validateWard}</Text>
                    ) : null}
                    <Input
                        label="Địa chỉ"
                        image={media.location}
                        placeHolder="Địa chỉ cụ thể"
                        placeHolderColor={Palette.color_ccc}
                        isRequired={true}
                        value={address}
                        setValue={setAddress}
                        maxLength={100}
                    />
                    {validateAddress.length > 0 ? (
                        <Text style={styles.validate}>{validateAddress}</Text>
                    ) : null}
                    <Text
                        style={[
                            styles.label,
                            {flex: 1, marginHorizontal: 20, marginTop: 10},
                        ]}>
                        {'Toà nhà nhận ship'}
                        <Text style={styles.required}>{' *'}</Text>
                    </Text>
                    <DropDownPicker
                        zIndex={3000}
                        items={listBuilding}
                        placeholder={'Vui lòng chọn toà nhà bạn muốn ship'}
                        searchable={true}
                        searchablePlaceholder="Tìm kiếm"
                        searchablePlaceholderTextColor="gray"
                        searchableError={() => <Text>{'Không tìm thấy'}</Text>}
                        containerStyle={{height: 40, marginHorizontal: 20, marginTop: 10}}
                        style={{
                            backgroundColor: Palette.white,
                            borderWidth: 0,
                            borderBottomWidth: 0.5,
                            borderBottomColor: Palette.white,
                        }}
                        selectedLabelStyle={{color:'#000'}}
                        activeLabelStyle={{color: 'red'}}
                        itemStyle={{
                            justifyContent: 'flex-start',
                            borderBottomWidth: 0.5,
                            borderBottomColor: Palette.color_ccc,
                        }}
                        dropDownStyle={{backgroundColor: Palette.white}}
                        placeholderStyle={{
                            color: Palette.color_ccc,
                            fontSize: RFValue(10, 580),
                        }}
                        onChangeItem={item => setBuilding(item.value)}
                    />
                    {validateBuilding.length > 0 ? (
                        <Text style={styles.validate}>{validateBuilding}</Text>
                    ) : null}
                    <View style={styles.groupImage}>
                        <Text style={styles.titleImage}>
                            <Text style={styles.required}>{'* '}</Text>
                            {'Vui lòng cung cấp chứng minh nhân dân hoặc căn cước công dân'}
                        </Text>
                        <Text style={styles.descriptionImage}>
                            {'(Chỉ chấp nhận ảnh chụp rõ nét, chụp cả 2 mặt của giấy tờ)'}
                        </Text>
                        <View style={styles.groupImagePicker}>
                            <ImageHolder
                                listImage={listImage}
                                setListValue={setListImage}
                                index={0}
                            />
                            <ImageHolder
                                listImage={listImage}
                                setListValue={setListImage}
                                index={1}
                            />
                            <ImageHolder
                                listImage={listImage}
                                setListValue={setListImage}
                                index={2}
                            />
                        </View>
                    </View>
                    {validateListImage.length > 0 ? (
                        <Text style={styles.validate}>{validateListImage}</Text>
                    ) : null}
                </View>
            </KeyboardAwareScrollView>
            <View style={{marginBottom: 20}}>
                <ButtonSubmit
                    title="Cập nhật"
                    color={[Palette.color_ff0, Palette.color_ff4]}
                    action={() =>
                        updateOwnerInfo(address, date, name, gender, ward,building, listImage)}
                    //address, date, name, ward,building ,listImage

                />
            </View>
        </SafeAreaView>
    );
};
export default UpdateOwnerHeaderInfoScreen;
