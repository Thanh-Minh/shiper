import {Palette} from "../../../theme/Palette";
import { StyleSheet } from 'react-native';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import { RFValue } from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
    marginBottom: getBottomSpace(),
  },
  form: {
    flex: 1,
    paddingBottom: 50,
    marginTop: 15,
    marginBottom: 50,
  },
  label: {
    color: Palette.color_545,
    fontSize: RFValue(10, 580),
  },
  containerDate: {
    height: 60,
    marginHorizontal: 20,
    marginTop: 20,
  },
  groupIconDate: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginRight: 13,
  },
  groupRadio: {
    flex: 1,
    maxWidth: 200,
    flexDirection: 'row',
    marginTop: 15,
  },
  groupIconRadio: {
    flex: 1,
    flexDirection: 'row',
  },
  iconRadio: {
    width: 15,
    height: 15,
    marginRight: 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  required: {
    fontWeight: '600',
    fontSize: RFValue(12, 580),
    color: Palette.color_ff3,
  },
  groupImage: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 20,
  },
  titleImage: {
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
  },
  descriptionImage: {
    fontSize: RFValue(9, 580),
    color: Palette.color_1f1,
    lineHeight: 16,
    fontStyle: 'italic',
    fontWeight: '300',
  },
  groupImagePicker: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-around',
  },
  validate: {
    color: 'red',
    marginTop: 9,
    marginHorizontal: 20,
  },
});
export default styles;
