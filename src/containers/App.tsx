import React, {} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import GlobalLoading from "../components/GlobalLoading";
import store from "../store";
import {Provider} from "react-redux";
import Navigation from "../navigation";
import {SafeAreaProvider} from "react-native-safe-area-context";

const App = () => {
    return (
        <SafeAreaProvider>
          <Provider store={store}>
            <Navigation/>
            <GlobalLoading/>
          </Provider>
        </SafeAreaProvider>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
});
export default App;
