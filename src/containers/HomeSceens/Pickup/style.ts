import {Platform, StyleSheet} from 'react-native';
import {Palette} from "../../../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: Palette.black,
        opacity: 0.6,
        zIndex: 0,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: Platform.OS === 'ios' ? 30 : 10,
    },
    modalContent: {
        width: '90%',
        height: 90,
        backgroundColor: Palette.white,
        borderRadius: 10,
        borderWidth: 0.6,
        borderColor: Palette.color_ccc,
        shadowColor: Palette.black,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.3,
        shadowRadius: 2,
    },
    openButtonNoLine: {
        padding: 10,
        zIndex: 1,
    },
    textStyle: {
        fontSize: RFValue(15, 580),
        textAlign: 'center',
    },
    option: {
        color: Palette.color_3c8,
    },
    openButton: {
        borderTopWidth: 0.5,
        borderTopColor: Palette.color_ccc,
        padding: 10,
        zIndex: 1,
    },
    modalCancel: {
        width: '90%',
        height: 50,
        marginTop: 10,
        backgroundColor: Palette.white,
        borderRadius: 10,
        borderWidth: 0.6,
        borderColor: Palette.color_ccc,
        shadowColor: Palette.black,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.3,
        shadowRadius: 2,
    },
    cancel: {
        color: Palette.color_ff2,
        fontWeight: '500',
    },
    itemHolder: {
        backgroundColor: Palette.white,

    },
    titleContent: {
        fontSize: RFValue(12, 580),
        fontWeight: 'bold',
        color: Palette.color_ff04,
        marginHorizontal: 8,

    },
    textContent: {
        fontSize: RFValue(12, 580),
        marginHorizontal: 8,
        marginTop: 7,
        color: Palette.black,
    },
    groupImage: {
        flex: 1,
        marginTop: 10,
        alignContent: 'stretch',
    },
    loading: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        left: 0,
    },
    containerImage: {
        width: 70,
        height: 70,
        borderWidth: 1,
        borderColor: Palette.color_c6c,
        borderStyle: 'dashed',
        borderRadius: 1,
        justifyContent: 'center',
        zIndex: 0,
        marginLeft:10,
        marginBottom:10
    },
    iconAdd: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    btnHolder: {
        width: 120,
        position: "absolute",
        right: 0,
        height:20
    },
});
