import React, {Dispatch, ReactElement, SetStateAction, useEffect, useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    Alert,
    Image,
    ScrollView, ActivityIndicator, GestureResponderEvent, Modal, RefreshControl,

} from 'react-native';
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import {Palette} from "../../../theme/Palette";
import styles from './style'
import media from "../../../assets/media";
import {useDispatch} from "react-redux";
import {Actions as DataAction} from "../../../redux/DataRedux";
import {createFormData} from "../../../lib/dataHelper";
import ImagePicker from "react-native-image-crop-picker";
import {code} from "../../../lib/codeHelpers";
import FastImage from "react-native-fast-image";
import env from "../../../config/Enviroment/env";
import {Actions as GlobalAction} from "../../../redux/GlobalRedux";
import {useNavigation} from "@react-navigation/native";
import {convert_dropdown_data} from "../../../lib/convertDataHelper";
import {BaseScreenProps} from "../../../@types/screen-type";
import {ScreenMap} from "../../../config/NavigationConfig";


const PopUp = ({
                   visible,
                   setVisible,
                   openCamera,
                   openGallery,
               }: {
    visible: boolean;
    setVisible: Dispatch<SetStateAction<boolean>>;
    openCamera: (event: GestureResponderEvent) => void;
    openGallery: (event: GestureResponderEvent) => void;
}) => {
    return (
        <Modal
            transparent={true}
            visible={visible}
            presentationStyle="overFullScreen">
            <View style={styles.overlay}/>
            <View style={styles.centeredView}>
                <View style={styles.modalContent}>
                    <TouchableOpacity
                        style={styles.openButtonNoLine}
                        onPress={openCamera}>
                        <Text style={[styles.textStyle, styles.option]}>{'Camera'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.openButton} onPress={openGallery}>
                        <Text style={[styles.textStyle, styles.option]}>{'Gallery'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.modalCancel}>
                    <TouchableOpacity
                        style={styles.openButtonNoLine}
                        onPress={() => {
                            setVisible(false);
                        }}>
                        <Text style={[styles.textStyle, styles.cancel]}>{'Cancel'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

const Item = ({item}: { item: Pickup }) => {
    const dispatch = useDispatch();
    const navigation=useNavigation();
    const [data,setData]=useState([])
    const [loading, setLoading] = useState(false);
    const [image, setImage] = useState(item.confirmImage);
    const [chooseVisible, setChooseVisible] = useState(false);
    const [chageAction, setChageAction] = useState(false)
    const [chage, setChage] = useState(false);
    const [status, setStatus] = useState(item.status)
    const uploadImage = (img: any) => {
        dispatch(
            DataAction.uploadImage(
                {formData: createFormData(img)},
                {
                    onBeginning: () => {
                        setLoading(false);
                    },
                    onSuccess: (response: any) => {
                        setImage(response.data[0]);
                        setChageAction(true)

                    },
                    onFailure: (error: any) => {
                        console.warn(error);
                    },
                    onFinish: () => {
                        setLoading(false);
                    },
                },
            ),
        );
    };
    const open_gallery = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openPicker({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    }

    const open_camera = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openCamera({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    };
    const put_Cart = (id: number, image: string) => {
        if (chageAction === true) {

            dispatch(
                DataAction.putcarts({
                        id: id,
                        imageUrl: image,

                    }, {
                        onBeginning: () => {

                        },
                        onSuccess: (data: any) => {
                            switch (data.errorCode) {
                                case code.success:
                                    setChage(true)

                            }

                        },
                        onFailure: () => {
                        },
                        onFinish: () => {
                        }
                    }
                )
            )
        }
    }

    return (
        <View style={styles.itemHolder}>

                <Text style={[styles.titleContent, {color: Palette.black, marginTop: 10}]}>
                    {'Sản phẩm: Đơn ' + item.orderCode}
                </Text>
            <Text style={styles.textContent}>{item.productName + ' - ' + item.packingType}</Text>
            <Text style={styles.textContent}>{'SL: ' + item.quantity}</Text>
            <Text style={styles.textContent}>{item.shopAddress}</Text>
            <Text style={styles.textContent}>{'Số điện thoại: ' + item.shopPhoneNumber}</Text>
            <View style={styles.groupImage}>
                {item.status ==3 ?(
                    <Image source={{uri: env.API_ENDPOINT + image}}  style={styles.containerImage}/>
                    ):(
                    <TouchableOpacity
                        onPress={() => (image ? null : setChooseVisible(true))}>
                        {loading ? (
                            <ActivityIndicator style={styles.loading} animating={true}/>
                        ) : null}
                        <View style={styles.containerImage}>

                            <FastImage
                                source={
                                    image
                                        ? {uri: env.API_ENDPOINT + image}
                                        : media.image
                                }
                                style={styles.iconAdd}
                                onLoadStart={() => (image ? setLoading(true) : null)}
                                onLoadEnd={() => (image ? setLoading(false) : null)}
                            />
                        </View>
                    </TouchableOpacity>
                )
                }
            </View>

            <View style={styles.btnHolder}>
                {item.status == 1 ? (image == null ? (<Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đang chuẩn bi'}</Text>)
                        : (!chage ? (
                                <ButtonSubmit title={"Lấy hàng"} color={chageAction
                                    ? [Palette.color_ff0, Palette.color_ff4]
                                    : [Palette.color_ccc, Palette.color_ccc]} isSmall={true}
                                              action={() => chageAction ? put_Cart(item.id, image) : null}
                                />) : (<Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>)
                        )
                ) : item.status == 2 ?
                    (!chage ? (
                            <ButtonSubmit title={"Lấy hàng"} color={chageAction
                                ? [Palette.color_ff0, Palette.color_ff4]
                                : [Palette.color_ccc, Palette.color_ccc]} isSmall={true}
                                          action={() => chageAction ? put_Cart(item.id, image) : null}
                            />) : (<Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đã lấy hàng'}</Text>)
                    ) : item.status == 3 ? (
                        <Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đã lấy hàng'}</Text>
                    ) : null
                }


            </View>
            <PopUp
                visible={chooseVisible}
                setVisible={setChooseVisible}
                openCamera={() => open_camera()}
                openGallery={() => open_gallery()}
            />
            <View style={{borderBottomWidth: 0.3, marginRight: 10, width: '90%', alignSelf: "center"}}/>
        </View>
    )

}

const Pickup =  (props: BaseScreenProps<ScreenMap.Pickup>): ReactElement => {
    const dispatch = useDispatch();
    const [data,setData]=useState([])
    const [refreshing, setRefreshing] = React.useState(false);
    const renderItem = ({item}: {item: Pickup}) => {
        return <Item item={item}/>;
    };
    const wait = (timeout:any) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(2000).then(() => setRefreshing(false));
    }, []);
    useEffect(() => {

        const unsubcribe = props.navigation.addListener('focus', () => {

            dispatch(
                DataAction.getPickup(
                    {},
                    {
                        onBeginning: () => {},
                        onSuccess: (response: any) => {
                            setRefreshing(true);
                            wait(2000).then(()=>setRefreshing(false))
                          setData(response.data)

                        },
                        onFailure: () => {},
                        onFinish: () => {},
                    },
                ),
            );
        });
        return unsubcribe;
    }, [dispatch]);

    return (
        <SafeAreaView style={styles.container}>

             <FlatList data={data} renderItem={renderItem} refreshControl={ <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}/>
        </SafeAreaView>

    );
}


export default Pickup;
