import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Palette} from "../../../theme/Palette";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    amountHolder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 20,
        margin: 10,
        backgroundColor: Palette.color_4a8,
        borderRadius: 6,
    },
    txtUser: {
        flex: 1,
        color: Palette.white,
        fontWeight: '700',
        fontSize: RFValue(15, 580),
    },
    txtUserAmount: {
        flex: 1,
        color: Palette.white,
        fontWeight: '700',
        fontSize: RFValue(15, 580),
        textAlign: 'right',
    },
    groupDateDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    dateRange: {
        fontSize: RFValue(11, 580),
        fontWeight: '400',
        alignSelf: 'center',
    },
    groupPickDate: {
        flexDirection: 'row',
    },
    dateStatisticIcon: {
        width: 21,
        height: 21,
        resizeMode: 'contain',
        marginHorizontal: 15,
        alignSelf: 'center',
    },
    dateHolder: {
        flex: 1,
        margin: 10,
        marginLeft: 20,
        marginRight: 10,
    },
    dateHolderTitle: {
        fontWeight: '600',
        fontSize: RFValue(13, 580),
    },
    dateButtonHolder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
    },
    btnHolder: {
        flex: 1,
        minWidth: 100,
        marginHorizontal: 15,
    },
    groupItemHolder: {
        flex: 1,
        marginTop: 5,
    },
    itemHolder: {
        flex: 1,
        margin: 10,
        padding: 15,
        borderRadius: 6,
        minHeight: 90,
        justifyContent: 'center',
        alignContent: 'center',
    },
    txtTitle: {
        flex: 1,
        fontSize: RFValue(11, 580),
        fontWeight: '500',
        color: Palette.white,
    },
    txtNumber: {
        flex: 1,
        fontSize: RFValue(13, 580),
        fontWeight: '500',
        color: Palette.white,
        marginTop: 10,
    },
    txtcomment:{
        flex: 1,
        fontSize: RFValue(12, 680),
        fontWeight: '500',
        color: Palette.white,
        marginTop: 10,
    },
    groupInfo: {
        flex: 1,
        marginTop: 10,
        marginLeft:10,
        marginRight:10,
        paddingLeft: 20,
        paddingRight:20,
        backgroundColor: Palette.white,
        borderRadius: 6,
    },
    groupInfoContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 5,
    },
    txtTitleContent: {
        fontSize: RFValue(11, 580),
        fontWeight: '300',

    },
    txtContent: {
        fontSize: RFValue(12, 580),
        fontWeight: '400',
    },
    groupRate: {
        flex: 1,
        marginLeft: 10,


    },
    starImage: {
        width: 40,
        height: 40,
        resizeMode: 'cover',
        marginHorizontal: 5,
    },
    groupStar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
    },
});
export default styles;
