import React, {useState} from 'react';
import {View, Text, StyleSheet, Alert, Image, SafeAreaView, FlatList} from 'react-native';
import media from "../../../../assets/media";
import styles from "./style";

const DATA = [
    {
        order_code: '#DH0007',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
    {
        order_code: '#DH0008',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
    {
        order_code: '#DH0009',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
    {
        order_code: '#DH0010',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
    {
        order_code: '#DH0011',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
    {
        order_code: '#DH0012',
        title:'Thịt bò Tây Ninh - 300g - 2',
        time: '10:30',
        date: '12/08/2020',
        address:' Nhà 822 Rain Bow,Linh Đàm, Hà nội',
        note: 'Ship bị chậm 30 phút',
    },
];


// @ts-ignore
const Item = ({order_code, title, time, date, address,note}) => {
    const [rate, setRate] = useState(2);





        return (
            <View style={styles.itemHolder}>
                <Text style={styles.txtOrder_code}>{'Đơn hàng : ' + order_code}</Text>
                <Text style={[styles.txttext, {marginTop: 0}]}>{title}</Text>
                <Text style={[styles.txttext, {marginTop: 0}]}>{time} {date}</Text>
                <Text
                    style={[styles.txttext, {marginTop: 0}]}>{'Địa chỉ: '+ address}</Text>
                <Text style={[styles.txttext, {marginTop: 0}]}>{'Lời nhắn: '+note}</Text>
                <View style={styles.groupStar}>
                    {[...Array(5)].map((el, i) => (
                        <Image
                            style={styles.starImage}
                            source={i <= rate - 1 ? media.star : media.star_With_Border}
                        />
                    ))}
                </View>
            </View>
        )
    }
;

const StatisticsDetail = () => {
    // @ts-ignore
    const renderItem = ({item}) => (
        <Item order_code={item.order_code}
              title={item.title}
              time={item.time}
              date={item.date}
              address={item.address}
              note={item.note}/>

    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.order_code}
            />
        </SafeAreaView>
    );
}
export default StatisticsDetail;
