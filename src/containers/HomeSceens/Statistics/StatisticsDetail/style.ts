import {StyleSheet} from 'react-native';
import {Palette} from "../../../../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    itemHolder: {
        flex: 1,
        marginTop: 10,
        padding: 16,
        backgroundColor: Palette.white,
        shadowColor: Palette.black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,


    },
    txtOrder_code: {
        fontSize: RFValue(16, 580),
        fontWeight: 'bold',
        marginBottom:10


    },
    txttext: {
        color: Palette.black,
        fontSize: RFValue(16, 680),
        fontWeight: "100",
        marginTop: 8,
        marginBottom: 8
    },
    groupStar: {
        flexDirection: "row",
        position: "absolute",
        right: 10,
        top:16
    },
    starImage: {
        width: 26,
        height: 26,
        resizeMode: 'cover',
        marginHorizontal: 2,
    },
})
