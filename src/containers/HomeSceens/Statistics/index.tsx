import media from "../../../assets/media";
import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {
    FlatList,
    Image,
    SafeAreaView,
    ScrollView,
    Text,
    View,
} from 'react-native';
import moment from 'moment';
import {RFValue} from 'react-native-responsive-fontsize';
import styles from './style';
import {Palette} from "../../../theme/Palette";
import TextLink from "../../../components/Form/TextLink";
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import {ScreenMap} from "../../../config/NavigationConfig";
import {useDispatch} from "react-redux";
import {Actions as DataAction} from "../../../redux/DataRedux";
import Numeral from 'numeral';
import NumberFormat from 'react-number-format';
import {numberWithDots} from "../../../lib/convertDataHelper";

const Item = ({
                  item,
              }: {
    item: {title: string; number: number; color: string};
}) => (

    <View style={[styles.itemHolder, {backgroundColor: item.color}]}>
        <Text style={styles.txtTitle}>{item.title}</Text>

        <Text style={styles.txtTitle}>{numberWithDots(item.number)}</Text>

    </View>
);

const StatisticScreen = (props: any): ReactElement => {
    const dispatch = useDispatch();
    const [successOrder,setSuccessOrder]= useState(0)
    const [cancelOrder,setCancelOrder]=useState(0)
   const [averageShippingTime,setAverageShippingTime]=useState(0)
    const [rate, setRate] = useState(5);
    const [dateStart, setDateStart] = useState(moment().format('DD/MM/YYYY'));
    const [dateEnd, setDateEnd] = useState(moment().format('DD/MM/YYYY'));
    const [chooseDateIndex, setChooseDateIndex] = useState(0);
    const [balanceAccount, setBalanceAccount] = useState(0);
    const [totalOrder,setTotalOrder]= useState(0)
    const [data, setData] = useState([
        {
            title: 'Thu nhập',
            number: 0,
            color: '#FF8B00',
        },
        {
            title: 'Tổng đơn',
            number: 0,
            color: '#03B22A',
        },

    ]);
    useEffect(() => {
        getDataStatistic(
            new Date(
                +dateStart.split('/')[2],
                +dateStart.split('/')[1] - 1,
                +dateStart.split('/')[0],
            ).setHours(0, 0, 0),
            new Date(
                +dateEnd.split('/')[2],
                +dateEnd.split('/')[1] - 1,
                +dateEnd.split('/')[0],
            ).setHours(23, 59, 0),

        );
    }, [dateStart, dateEnd]);
    const getDataStatistic = useCallback((dateStart: number, dateEnd: number) => {
        dispatch(
            DataAction.getDataStatistic(
                {startTime: dateStart, endTime: dateEnd},
                {
                    onBeginning: () => {},
                    onSuccess: (response: any) => {
                        const info = response.data;
                        setBalanceAccount(info.balanceAccount);
                        setAverageShippingTime(info.averageShippingTime)
                        data[0].number=info.revenue
                        data[1].number=info.totalOrder
                        setData([...data]);
                        setSuccessOrder(info.successOrder)
                        setCancelOrder(info.cancelOrder)
                        setTotalOrder(info.totalOrder)


                    },
                    onFailure: (error: any) => {
                        console.log(error);
                    },
                    onFinish: () => {},
                },
            ),
        );
    }, []);

    const renderItem = ({
                            item,
                        }: {
        item: {title: string; number: number; color: string};
    }) => {
        return <Item item={item} />;
    };
    const setIndexButton = useCallback(index => {
        setChooseDateIndex(index);
        switch (index) {
            case 0:
                setDateStart(moment().format('DD/MM/YYYY'));
                setDateEnd(moment().format('DD/MM/YYYY'));
                break;
            case 1:
                setDateStart(
                    moment()
                        .startOf('week')
                        .format('DD/MM/YYYY'),
                );
                setDateEnd(moment().format('DD/MM/YYYY'));
                break;
            case 2:
                setDateStart(
                    moment()
                        .startOf('month')
                        .format('DD/MM/YYYY'),
                );
                setDateEnd(moment().format('DD/MM/YYYY'));
                break;

        }
    }, []);


    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container}>
                <View style={styles.amountHolder}>
                    <Text style={styles.txtUser}>{'Tài khoản'}</Text>

                    <Text style={styles.txtUserAmount}>{numberWithDots(balanceAccount) +' đ'}</Text>
                </View>
                <View style={styles.dateHolder}>
                    <View style={styles.groupDateDetail}>
                        <Text style={styles.dateHolderTitle}>{'Thống kê'}</Text>
                        <View style={styles.groupPickDate}>
                            <Text style={styles.dateRange}>
                                {dateStart === dateEnd
                                    ? dateStart
                                    : dateStart + ' - ' + dateEnd}
                            </Text>
                            <Image
                                style={styles.dateStatisticIcon}
                                source={media.calendar}
                            />
                        </View>

                    </View>
                    <View style={styles.dateButtonHolder}>
                        <View style={styles.btnHolder}>
                            <ButtonSubmit
                                title="Hôm nay"
                                color={
                                    chooseDateIndex == 0
                                        ? [Palette.color_f66, Palette.color_f66]
                                        : [Palette.color_9b9, Palette.color_9b9]
                                }
                                isSmall={true}
                                action={() => {
                                    setIndexButton(0);
                                }}
                            />
                        </View>
                        <View style={styles.btnHolder}>
                            <ButtonSubmit
                                title="Tuần"
                                color={
                                    chooseDateIndex == 1
                                        ? [Palette.color_f66, Palette.color_f66]
                                        : [Palette.color_9b9, Palette.color_9b9]
                                }
                                isSmall={true}
                                action={() => {
                                    setIndexButton(1);
                                }}
                            />

                        </View>
                        <View style={styles.btnHolder}>
                            <ButtonSubmit
                                title="Tháng"
                                color={
                                    chooseDateIndex == 2
                                        ? [Palette.color_f66, Palette.color_f66]
                                        : [Palette.color_9b9, Palette.color_9b9]
                                }
                                isSmall={true}
                                action={() => {
                                    setIndexButton(2);
                                }}
                            />

                        </View>
                    </View>
                </View>
                <View style={styles.groupItemHolder}>
                    <FlatList data={data} numColumns={2} renderItem={renderItem} />
                    <View style={[styles.itemHolder, {backgroundColor: '#6A6ACC'}]}>
                        <Text style={styles.txtTitle}>{"Trung bình thời gian giao hàng"}</Text>
                        <Text style={[styles.txtNumber,{fontSize:26}]}>{averageShippingTime +' Phút'}</Text>
                       <Text style={styles.txtcomment}>{'Thời gian bị chậm hơn 120 phút/ tháng sẽ tính phạt kpi'}</Text>
                    </View>
                </View>
                <View style={styles.groupInfo}>
                    <View style={styles.groupInfoContent}>
                        <Text style={styles.txtTitleContent}>{'Tổng đơn đặt: '}</Text>
                        <Text style={styles.txtContent}>{totalOrder}</Text>
                    </View>
                    <View style={styles.groupInfoContent}>
                        <Text style={styles.txtTitleContent}>{'Đơn thành công: '}</Text>
                        <Text style={styles.txtContent}>{successOrder}</Text>
                    </View>
                    <View style={styles.groupInfoContent}>
                        <Text style={styles.txtTitleContent}>{'Đơn đã hủy:'}</Text>
                        <Text style={styles.txtContent}>{cancelOrder}</Text>
                    </View>
                </View>
                <View style={styles.groupRate}>
                    <Text style={[styles.txtTitleContent,{marginLeft:20}]}>{'Đánh giá'}</Text>
                    <View style={styles.groupStar}>
                        {[...Array(5)].map((el, i) => (
                            <Image
                                style={styles.starImage}
                                source={i <= rate - 1 ? media.star : media.star_With_Border}
                            />
                        ))}
                    </View>
                    <TextLink
                        textTitle="(2 đánh giá/ hôm nay) "
                        textLink="xem chi tiết..."
                        fontSize={RFValue(10, 580)}
                        color={Palette.color_0f9}
                        action={() => props.navigation.navigate(ScreenMap.StatisticsDetail, {})}

                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};
export default StatisticScreen;
