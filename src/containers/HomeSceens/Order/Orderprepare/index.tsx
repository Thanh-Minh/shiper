import React, {ReactElement, useState} from 'react';
import {View, Text, SafeAreaView, FlatList, ScrollView, TouchableOpacity, RefreshControl} from "react-native";

import {ScreenMap, ScreenParams} from "../../../../config/NavigationConfig";
import styles from "./style";

import {useNavigation} from '@react-navigation/native';
import moment from "moment";


const Item = ({item, jumpTo}: { item: OrdersItem, jumpTo: any }) => {
    const navigation = useNavigation();
    console.log(item)

    return (
        <TouchableOpacity
            style={styles.itemHolder}
            onPress={() => {
                navigation.navigate('BottomTabStack', {
                    screen: 'Order',
                    params: {
                        screen: ScreenMap.OrderDetail,
                        params: {item: item, jumpTo: jumpTo},
                    },
                });
            }}>
            <View style={styles.contentHolder}>
                <Text style={[styles.textContent, styles.titleContent]}>
                    {'Đơn hàng: ' + item.code}
                </Text>

                <Text style={styles.textContent}>{'Số shop: ' + item.totalShop}</Text>
                <Text style={styles.textContent}>{moment(new Date(item.createdTime)).format('HH:mm  DD/MM/YYYY')}</Text>
                {item.deliveryNote ? (
                    <Text style={[styles.textContent, styles.note]}>
                        {`Lưu ý: ${item.deliveryNote}`}
                    </Text>
                ) : null}
            </View>
        </TouchableOpacity>
    );
};
const OrderPrepare = ({data,jumpTo}: { data: any,jumpTo:any }): ReactElement => {

    const renderItem = ({item}: { item: OrdersItem }) => {
        return <Item item={item} jumpTo={jumpTo}/>;
    };
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout: any) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
data();
        wait(2000).then(() => setRefreshing(false));
    }, []);


    return (
        <SafeAreaView style={styles.container}>

                <FlatList data={data} renderItem={renderItem} keyExtractor={item => item.code} refreshControl={ <RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}/>

        </SafeAreaView>
    );
};

export default OrderPrepare;
