import React, {ReactElement} from 'react';
import {View, Text, SafeAreaView, ScrollView, FlatList, TouchableOpacity, RefreshControl} from "react-native";
import {useNavigation} from "@react-navigation/native";
import styles from "../Orderprepare/style";
import moment from "moment";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import {Palette} from "../../../../theme/Palette";
import {takePutCartsSuccess} from "../../../../saga/DataSaga";
import {useDispatch} from "react-redux";
import {Actions as DataAction} from '../../../../redux/DataRedux';
import {code} from "../../../../lib/codeHelpers";
import {ScreenMap} from "../../../../config/NavigationConfig";


const Item = ({item}: { item: OrdersItem,  }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            style={styles.itemHolder}
            onPress={() => {
                navigation.navigate('BottomTabStack', {
                    screen: 'Order',
                    params: {
                        screen: ScreenMap.OrderDetail,
                        params: {item: item},
                    },
                });
            }}>
            <View style={styles.contentHolder}>
                <Text style={[styles.textContent, styles.titleContent]}>
                    {'Đơn hàng: ' + item.code}
                </Text>

                <Text style={styles.textContent}>{'Số shop: ' + item.totalShop}</Text>
                <Text style={styles.textContent}>{moment(new Date(item.createdTime)).format('HH:mm  DD/MM/YYYY')}</Text>
                {item.deliveryNote ? (
                    <Text style={[styles.textContent, styles.note]}>
                        {`Lưu ý: ${item.deliveryNote}`}
                    </Text>
                ) : null}
            </View>
        </TouchableOpacity>
    );
};
const OrderOnShip = ({data,}: { data: any }): ReactElement => {

    const renderItem = ({item}: { item: OrdersItem }) => {
        return <Item item={item} />;
    };
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout: any) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
data();
        wait(2000).then(() => setRefreshing(false));
    }, []);

    return (
        <SafeAreaView style={styles.container}>
                <FlatList data={data} renderItem={renderItem} refreshControl={ <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}/>

        </SafeAreaView>
    );
};

export default OrderOnShip;
