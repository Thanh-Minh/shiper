import React, {ReactElement, useState} from 'react';
import {View, Text, SafeAreaView, FlatList, ScrollView, TouchableOpacity, RefreshControl} from "react-native";

import styles from "./style";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import {Palette} from "../../../../theme/Palette";
import moment from "moment";


const Item = ({item}: { item: OrdersItem }) => {

    return (
        <View style={styles.itemHolder}>
            <View style={styles.contentHolder}>
                <Text style={[styles.textContent, styles.titleContent]}>
                    {'Đơn hàng: ' + item.code}
                </Text>

                <Text style={styles.textContent}>{'Số shop: ' + item.totalShop}</Text>
                <Text style={styles.textContent}>{moment(new Date(item.createdTime)).format('HH:mm  DD/MM/YYYY')}</Text>
                {item.deliveryNote ? (
                    <Text style={[styles.textContent, styles.note]}>
                        {`Lưu ý: ${item.deliveryNote}`}
                    </Text>
                ) : null}
            </View>
            <View style={{position: "absolute", right: 20, top: 10}}>

                <Text style={{
                    color: Palette.color_33c,
                }}>{'Đã giao hàng'}</Text>

            </View>
        </View>
    );
};
const OrderComplete = ({data, jumpTo}: { data: any, jumpTo: any }): ReactElement => {
    const renderItem = ({item}: { item: NewOrderItem }) => {
        return <Item item={item}/>;
    };
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout: any) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
data();
        wait(2000).then(() => setRefreshing(false));
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList data={data} renderItem={renderItem}
                      refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}/>

        </SafeAreaView>
    );
};

export default OrderComplete;
