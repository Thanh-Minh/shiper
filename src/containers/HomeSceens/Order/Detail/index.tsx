import React, {Dispatch, ReactElement, SetStateAction, useCallback, useEffect, useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Image,
    FlatList,
    TouchableOpacity,
    GestureResponderEvent, Modal, ActivityIndicator, Alert
} from 'react-native';
import {BaseScreenProps} from "../../../../@types/screen-type";
import {ScreenMap} from "../../../../config/NavigationConfig";
import styles from "./style";
import {Palette} from "../../../../theme/Palette";
import media from "../../../../assets/media";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import ImageHolder from "../../../../components/ImageHolder";
import {Actions as DataAction} from '../../../../redux/DataRedux';
import {useDispatch} from "react-redux";
import {code} from "../../../../lib/codeHelpers";
import ImagePicker from "react-native-image-crop-picker";
import FastImage from "react-native-fast-image";
import env from "../../../../config/Enviroment/env";
import {createFormData} from "../../../../lib/dataHelper";
import {render} from "enzyme";
import {RFValue} from "react-native-responsive-fontsize";


const PopUp = ({
                   visible,
                   setVisible,
                   openCamera,
                   openGallery,
               }: {
    visible: boolean;
    setVisible: Dispatch<SetStateAction<boolean>>;
    openCamera: (event: GestureResponderEvent) => void;
    openGallery: (event: GestureResponderEvent) => void;
}) => {
    return (
        <Modal
            transparent={true}
            visible={visible}
            presentationStyle="overFullScreen">
            <View style={styles.overlay}/>
            <View style={styles.centeredView}>
                <View style={styles.modalContent}>
                    <TouchableOpacity
                        style={styles.openButtonNoLine}
                        onPress={openCamera}>
                        <Text style={[styles.textStyle, styles.option]}>{'Camera'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.openButton} onPress={openGallery}>
                        <Text style={[styles.textStyle, styles.option]}>{'Gallery'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.modalCancel}>
                    <TouchableOpacity
                        style={styles.openButtonNoLine}
                        onPress={() => {
                            setVisible(false);
                        }}>
                        <Text style={[styles.textStyle, styles.cancel]}>{'Cancel'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

const Item = ({item,index}: { item: Pickup ,index:number}) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [image, setImage] = useState(item.confirmImage);
    const [chooseVisible, setChooseVisible] = useState(false);
    const [chageAction, setChageAction] = useState(false)
    const [chage, setChage] = useState(false);
    const [status, setStatus] = useState(item.status)
    const uploadImage = (img: any) => {
        dispatch(
            DataAction.uploadImage(
                {formData: createFormData(img)},
                {
                    onBeginning: () => {
                        setLoading(false);
                    },
                    onSuccess: (response: any) => {
                        setImage(response.data[0]);
                        setChageAction(true)

                    },
                    onFailure: (error: any) => {
                        console.warn(error);
                    },
                    onFinish: () => {
                        setLoading(false);
                    },
                },
            ),
        );
    };
    const open_gallery = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openPicker({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    }

    const open_camera = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openCamera({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    };
    const put_Cart = (id: number, image: string) => {
        if (chageAction === true) {

            dispatch(
                DataAction.putcarts({
                        id: id,
                        imageUrl: image,

                    }, {
                        onBeginning: () => {

                        },
                        onSuccess: (data: any) => {
                            switch (data.errorCode) {
                                case code.success:
                                    setChage(true)

                            }

                        },
                        onFailure: () => {
                        },
                        onFinish: () => {
                        }
                    }
                )
            )
        }
    }



    return (
        <View style={styles.itemHolder}>
            <Text style={[styles.titleContent, {color: Palette.black, marginTop: 10}]}>
                {'Sản phẩm '+`${index+1}`+': Đơn ' + item.orderCode}

            </Text>
            <Text style={styles.textContent}>{item.productName + ' - ' + item.packingType}</Text>
            <Text style={styles.textContent}>{'SL: ' + item.quantity}</Text>
            <Text style={styles.textContent}>{item.shopAddress}</Text>
            <Text style={styles.textContent}>{'Số điện thoại: ' + item.shopPhoneNumber}</Text>
            <View style={styles.groupImage}>
                <TouchableOpacity
                    onPress={() => (image ? null : setChooseVisible(true))}>
                    {loading ? (
                        <ActivityIndicator style={styles.loading} animating={true}/>
                    ) : null}
                    <View style={styles.containerImage}>
                        <FastImage
                            source={
                                image
                                    ? {uri: env.API_ENDPOINT + image}
                                    : media.image
                            }
                            style={styles.iconAdd}
                            onLoadStart={() => (image ? setLoading(true) : null)}
                            onLoadEnd={() => (image ? setLoading(false) : null)}
                        />
                    </View>
                </TouchableOpacity>
            </View>

            <View style={styles.btnHolder}>
                {item.status == 1 ? (image == null ? (<Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đang chuẩn bi'}</Text>)
                        : (!chage ? (
                                <ButtonSubmit title={"Lấy hàng"} color={chageAction
                                    ? [Palette.color_ff0, Palette.color_ff4]
                                    : [Palette.color_ccc, Palette.color_ccc]} isSmall={true}
                                              action={() => chageAction ? put_Cart(item.id, image) : null}
                                />) : (<Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>)
                        )
                ) : item.status == 2 ?
                    (!chage ? (
                            <ButtonSubmit title={"Lấy hàng"} color={chageAction
                                ? [Palette.color_ff0, Palette.color_ff4]
                                : [Palette.color_ccc, Palette.color_ccc]} isSmall={true}
                                          action={() => chageAction ? put_Cart(item.id, image) : null}
                            />) : (<Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đã lấy hàng'}</Text>)
                    ) : item.status == 3 ? (
                        <Text style={{
                            color: Palette.color_33c,
                            position: "absolute",
                            right: 16,
                            top: 10
                        }}>{'Đã lấy hàng'}</Text>
                    ) : null
                }


            </View>
            <PopUp
                visible={chooseVisible}
                setVisible={setChooseVisible}
                openCamera={() => open_camera()}
                openGallery={() => open_gallery()}
            />
            <View style={{borderBottomWidth: 0.3, marginRight: 10, width: '90%', alignSelf: "center"}}/>
        </View>
    )
}

const DetailOrderScreen = (
    props: BaseScreenProps<ScreenMap.OrderDetail>
): ReactElement => {
    const order_id = props.route.params.item.id;
    const dispatch = useDispatch();
    const [image, setImage] = useState('');
    const [chooseVisible, setChooseVisible] = useState(false);
    const [data, setData] = useState([])
    const [codeOrder, setCodeOrder] = useState('')
    const [totalShop, setTotalShop] = useState(0)
    const [deliveryAddress, setDeliveryAddress] = useState('')
    const [loading, setLoading] = useState(false);
    const [status, setStatus] = useState(0)
    const [deliveryPhoneNumber, setDeliveryPhoneNumber] = useState('')
    const [chageAction, setChageAction] = useState(false)
    const [uid, setuId] = useState(0)
    const uploadImage = (img: any) => {
        dispatch(
            DataAction.uploadImage(
                {formData: createFormData(img)},
                {
                    onBeginning: () => {
                        setLoading(false);
                    },
                    onSuccess: (response: any) => {
                        setImage(response.data[0]);
                        setChageAction(true)

                    },
                    onFailure: (error: any) => {
                        console.warn(error);
                    },
                    onFinish: () => {
                        setLoading(false);
                    },
                },
            ),
        );
    }
    const open_gallery = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openPicker({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    }

    const open_camera = () => {
        setChooseVisible(false);
        setTimeout(() => {
            ImagePicker.openCamera({
                width: 300,
                height: 400,
                cropping: true,
            })
                .then(img => {
                    uploadImage(img);
                })
                .catch(err => {
                    console.warn(err);
                });
        }, 200);
    };
    const put_cart = (id: number, image: string) => {
        if (chageAction === true) {

            dispatch(
                DataAction.putcartssuccess({
                        id: id,
                        imageUrl: image,

                    },
                    {

                        onBeginning: () => {

                        },
                        onSuccess: (data: any) => {
                            switch (data.errorCode) {
                                case code.success:
                                    props.navigation.goBack();
                            }

                        },
                        onFailure: (error:any) => {
                            Alert.alert(JSON.stringify(error))
                        },
                        onFinish: () => {
                        }
                    }
                )
            )
        }
    }
    // ok nhe 1 ly tra sua chan trau full topping :)) ship toi so 5 lo 5a trung yen 9 trung hoa cau giay ha noi
    useEffect(() => {
        const unsubcribe = props.navigation.addListener('focus', () => {
            dispatch(DataAction.detailOrder(
                {id: order_id},
                {
                    onBeginning: () => {

                    },
                    onSuccess: (response: any) => {

                        switch (response.errorCode) {
                            case code.success:
                                const orderInfo = response.data
                                setCodeOrder(orderInfo.code)
                                setTotalShop(orderInfo.totalShop)
                                setDeliveryAddress(orderInfo.deliveryAddress)
                                setData(orderInfo.cartForShipperDtos)
                                setDeliveryPhoneNumber(orderInfo.deliveryPhoneNumber)
                                setImage(orderInfo.confirmImage)
                                setStatus(orderInfo.status)
                                setuId(orderInfo.id)

                                break;
                        }

                    },
                    onFailure: (error: any) => {

                    },
                    onFinish: () => {
                    },
                }))

        });
        return unsubcribe;
    }, [dispatch, props.navigation]);
    const renderItem = ({item, index}: { item: cartForShipperDtos, index: number }) => (
        <Item item={item} index={index}/>
    )


    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.itemHolder}>
                    <Text style={[styles.textContent, styles.titleContent]}>{'Đơn hàng: ' + codeOrder}</Text>
                    <Text style={[styles.textContent, {color: Palette.color_3a3}]}>{'Số shop: ' + totalShop}</Text>
                    <Text
                        style={[styles.textContent, {color: Palette.black}]}>{'Số điện thoại: ' + deliveryPhoneNumber}</Text>
                    <Text
                        style={[styles.textContent, {color: Palette.black}]}>{'Địa chỉ: ' + deliveryAddress}</Text>

                </View>
                <View>
                    <FlatList data={data}
                              renderItem={renderItem}
                    />
                </View>
                <View style={{
                    borderBottomWidth: 1,
                    marginRight: 10,
                    width: '94%',
                    alignSelf: "center",

                    marginLeft: 8
                }}/>
                <View style={{backgroundColor: Palette.white,}}>

                    <View style={styles.groupImage}>{status === 3
                        ? (
                            <View style={{marginBottom:10}}>
                                <Text style={[styles.titleContent, {
                                    color: Palette.black,
                                    marginTop: 10
                                }]}>{'Giao hàng:'}</Text>
                                <TouchableOpacity
                                    onPress={() => (image ? null : setChooseVisible(true))}>
                                    {loading ? (
                                        <ActivityIndicator style={styles.loading} animating={true}/>
                                    ) : null}
                                    <View style={styles.containerImage}>
                                        <FastImage
                                            source={
                                                image
                                                    ? {uri: env.API_ENDPOINT + image}
                                                    : media.image
                                            }
                                            style={styles.iconAdd}
                                            onLoadStart={() => (image ? setLoading(true) : null)}
                                            onLoadEnd={() => (image ? setLoading(false) : null)}
                                        />
                                    </View>
                                </TouchableOpacity>
                                <ButtonSubmit title={"Giao hàng"} color={chageAction
                                    ? [Palette.color_ff0, Palette.color_ff4]
                                    : [Palette.color_ccc, Palette.color_ccc]}
                                              action={() => (chageAction ? put_cart(uid,image) : null)}

                                />

                            </View>
                        )
                        : null}

                    </View>
                </View>


                <PopUp
                    visible={chooseVisible}
                    setVisible={setChooseVisible}
                    openCamera={() => open_camera()}
                    openGallery={() => open_gallery()}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

export default DetailOrderScreen;
