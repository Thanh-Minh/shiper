import React, {ReactElement, useState} from 'react';
import {View, Text, SafeAreaView, FlatList, RefreshControl} from "react-native";
import styles from "./style";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import {Palette} from "../../../../theme/Palette";
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import {useDispatch} from "react-redux";
import {Actions as DataAction} from '../../../../redux/DataRedux';
import {code} from "../../../../lib/codeHelpers";




const Item = ({item,jumpTo}: {
    item: NewOrderItem
    jumpTo:any
}) => {
    const dispatch = useDispatch();
    const takeCompleteOrder = ()=>{
        dispatch(
            DataAction.completeOrder(
                {id: item.id},
                {
                    onBeginning: () => {
                    },
                    onSuccess: (response: any) => {
                        switch (response.errorCode) {
                            case code.success:
                                jumpTo('orderprepare');
                                break;
                        }

                    },
                    onFailure: (error: any) => {
                        console.log(error);
                    },
                    onFinish: () => {
                    },
                }
            )
        )
    }
    return (
        <View style={styles.item}>
            <View style={styles.contentHolder}>
                <Text style={[styles.textContent, styles.titleContent]}>
                    {'Đơn hàng: ' + item.code}
                </Text>

                <Text style={styles.textContent}>{'Số shop: '+item.totalShop}</Text>
                <Text style={styles.textContent}>{moment(new Date(item.createdTime)).format('HH:mm  DD/MM/YYYY')}</Text>
                {item.deliveryNote ? (
                    <Text style={[styles.textContent, styles.note]}>
                        {`Lưu ý: ${item.deliveryNote}`}
                    </Text>
                ) : null}
            </View>
            <View style={styles.btnHolder}>
                <ButtonSubmit title={"Nhận"} color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
action={()=>takeCompleteOrder()}
                />
            </View>

        </View>
    )
};




const NewOrder = ({data,jumpTo}: {data: any,jumpTo:any}): ReactElement => {
    const renderItem = ({item}: {item: NewOrderItem}) => {
        return <Item item={item} jumpTo={jumpTo}/>;
    };
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout:any) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        data();
        wait(2000).then(() => setRefreshing(false));
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.code}
                refreshControl={ <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
            />

        </SafeAreaView>
    );
}

export default NewOrder;
