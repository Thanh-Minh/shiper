import React, {ReactElement, useEffect, useState,memo} from 'react';
import {Text, View} from 'react-native';
import {BaseScreenProps} from "../../../@types/screen-type";
import {ScreenMap} from "../../../config/NavigationConfig";
import NewOrder from "./NewOrder";
import OrderPrepare from "./Orderprepare";
import OrderComplete from "./Ordercomplete";
import {screenWidth} from "../../../config/ScreenConfig";
import OrderOnShip from "./Orderonship";
import {SceneMap, TabBar, TabView} from "react-native-tab-view";
import {Palette} from "../../../theme/Palette";
import styles from './style'
import {useDispatch} from "react-redux";
import {Actions as DataAction} from '../../../redux/DataRedux';
import {Actions as GlobalAction} from '../../../redux/GlobalRedux';
import {RFValue} from "react-native-responsive-fontsize";
import {useNavigation} from "@react-navigation/native";

const initialLayout = {width: screenWidth,};

const Order = (

): ReactElement => {
   const navigation=useNavigation();
    const dispatch = useDispatch();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        {key: 'ordernew', title: 'Đơn mới'},
        {key: 'orderprepare', title: 'Đang lấy'},
        {key: 'orderonship', title: 'Đang giao'},
        {key: 'ordercomplete', title: 'Đã giao'},
    ])
    const [data, setData] = useState([])
    const [data1,setData1]= useState([])

    useEffect(() => {
        const unsubcribe = navigation.addListener('focus', () => {
            const newOrder = 1;

            getList(newOrder)


        });
        return unsubcribe;
    }, [dispatch, navigation]);

    const getList = (index: number) => {
        dispatch(
            DataAction.getListOrder(
                {status: index},
                {
                    onBeginning: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: true}));
                    },
                    onSuccess: (response: any) => {
                        setData(response.data)
                    },
                    onFailure: (error: any) => {
                        console.log(error)
                    },
                    onFinish: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: false}))
                    }
                }
            )
        )
    }
    const getListPrepare = (index: number,sort:any) => {
        dispatch(
            DataAction.prepareOrder(
                {status: index,sort:sort},
                {
                    onBeginning: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: true}));
                    },
                    onSuccess: (response: any) => {

                        setData1(response.data)

                    },
                    onFailure: (error: any) => {
                        console.log(error)
                    },
                    onFinish: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: false}))
                    }
                }
            )
        )
    }


    const onChangetab = (index:number) =>{
        setIndex(index);
        setData([]);
        switch (index){
            case 0:
                getList(1)
                break;
            case 1:
                getList(1)
                getListPrepare(1,'createdTime,asc')
                break
            case 2:
                getListPrepare(3,'createdTime,asc')
                break;
            case 3:
                getListPrepare(4,'createdTime,desc')
                break;
        }

    }

    const renderScene = ({route,jumpTo}:{route:any,jumpTo:any})=>{
        switch (route.key){
            case 'ordernew':
                return <NewOrder data={data} jumpTo={jumpTo}/>
            case 'orderprepare':
                return <OrderPrepare data={data1} jumpTo={jumpTo} />
            case 'orderonship':
                return <OrderOnShip data={data1} />
            case 'ordercomplete':
                return <OrderComplete data={data1} jumpTo={jumpTo}/>
            default:
                return null;
        }
    }

    const renderTabBar = (props: any) => (
        <TabBar
            {...props}
            getLabelText={({route}) => route.title}
            inactiveColor={Palette.black}
            activeColor={Palette.color_e91}
            indicatorStyle={{backgroundColor: Palette.white}}
            style={{backgroundColor: Palette.white,}}
            renderLabel={({route,focused,color})=>(
                <Text style={{color,fontSize:RFValue(10,580)}}>{route.title}</Text>
            )}

        />
    );

    return (
        <View style={styles.container}>
            <TabView
                renderTabBar={renderTabBar}
                navigationState={{index, routes}}
                renderScene={renderScene}
                onIndexChange={index => onChangetab(index)}
                initialLayout={initialLayout}
            />
        </View>

    );

}

export default memo(Order);
