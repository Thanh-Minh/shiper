import {StyleSheet} from 'react-native';
import {RFValue} from "react-native-responsive-fontsize";
import {Palette} from "../../../theme/Palette";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    containerScroll: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
        marginBottom: 10,
    },
    contentHolder: {
        flex: 0.8,
    },
    itemHolder: {
        flex: 0.5,
        marginTop: 10,
        marginBottom: 2,
        padding: 10,
        backgroundColor: Palette.white,
        shadowColor: Palette.black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    textContent: {
        fontSize: RFValue(10, 580),
        marginHorizontal: 8,
        marginTop: 7,
        color: Palette.color_3a3,
    },
    note: {
        color: Palette.color_ff4f,
        fontWeight: '600',
    },
    titleContent: {
        fontSize: RFValue(12, 580),
        fontWeight: 'bold',
    },
    btnHolder: {
        flex: 0.5,
        width: 150,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'flex-end',
    },
});
export default styles;
