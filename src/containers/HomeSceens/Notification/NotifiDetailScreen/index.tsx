
import React, {ReactElement} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import styles from './style';
import {ScreenMap} from "../../../../config/NavigationConfig";
import {BaseScreenProps} from "../../../../@types/screen-type";
import media from "../../../../assets/media";

const NotificationDetailScreen = (
    props: BaseScreenProps<ScreenMap.NotificationDetail>,
): ReactElement => {
    const item = props.route.params.item;
    return (
        <SafeAreaView style={styles.container}>
            <View style={[styles.infoHolder]}>
                <View style={styles.groupInfoAvatar}>
                    <View style={styles.groupAvatar}>
                        <Image source={media.avatar_default} style={styles.avatar} />
                        <View style={styles.groupTitle}>
                            <Text style={styles.txtTitle}>{item.title}</Text>
                            <Text style={styles.txtDate}>{item.send_date}</Text>
                        </View>
                    </View>
                </View>
                <ScrollView style={styles.groupContent}>
                    <Text style={styles.txtContent}>{item.content}</Text>
                    <Text style={styles.txtContent}>{item.create_date}</Text>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};
export default NotificationDetailScreen;
