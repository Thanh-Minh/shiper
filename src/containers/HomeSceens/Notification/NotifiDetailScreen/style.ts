import {Palette} from "../../../../theme/Palette";
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    infoHolder: {
        flex: 1,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 2,
        padding: 15,
        borderRadius: 10,
        backgroundColor: Palette.white,
        width: '95%',
        shadowColor: Palette.black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    groupInfoAvatar: {
        flexDirection: 'row',
    },
    groupAvatar: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 10,
    },
    avatar: {
        width: 45,
        height: 45,
        resizeMode: 'contain',
    },
    groupTitle: {
        marginLeft: 10,
    },
    txtTitle: {
        fontSize: RFValue(12, 580),
        fontWeight: '500',
        marginBottom: 7,
    },
    txtDate: {
        fontSize: RFValue(10, 580),
        color: Palette.color_234,
    },
    groupContent: {
        marginTop: 10,
    },
    txtContent: {
        fontSize: RFValue(10, 580),
        color: Palette.color_545,
        lineHeight: 17,
        fontWeight: '400',
    },
});
export default styles;
