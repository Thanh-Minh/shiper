import media from "../../../assets/media";
import React, {ReactElement} from 'react';
import {
    FlatList,
    Image,
    SafeAreaView,
    ScrollView,
    Text,
    View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import styles from './style';
import {BaseScreenProps} from "../../../@types/screen-type";
import {ScreenMap} from "../../../config/NavigationConfig";
const DATA = [
    {
        id: '1',
        title: 'Thanh toán thành công',
        send_date: '08:02  07/09/2020',
        content:
            'Khách hàng Tranxuankien đã thanh toán thành công số tiền 176,694 VND cho Vinmart 16 Cau Giay, lúc ',
        create_date: '30/08/2020 08:00:00',
        isRead: false,
    },
    {
        id: '2',
        title: 'Thanh toán thành công',
        send_date: '08:02  07/09/2020',
        content:
            'Khách hàng Tranxuankien đã thanh toán thành công số tiền 176,694 VND cho Vinmart 16 Cau Giay, lúc ',
        create_date: '30/08/2020 08:00:00',
        isRead: false,
    },
    {
        id: '3',
        title: 'Thanh toán thành công',
        send_date: '08:02  07/09/2020',
        content:
            'Khách hàng Tranxuankien đã thanh toán thành công số tiền 176,694 VND cho Vinmart 16 Cau Giay, lúc ',
        create_date: '30/08/2020 08:00:00',
        isRead: false,
    },
    {
        id: '4',
        title: 'Thanh toán thành công',
        send_date: '08:02  07/09/2020',
        content:
            'Khách hàng Tranxuankien đã thanh toán thành công số tiền 176,694 VND cho Vinmart 16 Cau Giay, lúc ',
        create_date: '30/08/2020 08:00:00',
        isRead: true,
    },
    {
        id: '5',
        title: 'Thanh toán thành công',
        send_date: '08:02  07/09/2020',
        content:
            'Khách hàng Tranxuankien đã thanh toán thành công số tiền 176,694 VND cho Vinmart 16 Cau Giay, lúc ',
        create_date: '30/08/2020 08:00:00',
        isRead: true,
    },
];

const Item = ({item}: {item: NotifyItem}) => (
    <View style={[styles.infoHolder, item.isRead ? styles.blur : null]}>
        <View style={styles.groupInfoAvatar}>
            <View style={styles.groupAvatar}>
                <Image source={media.avatar_default} style={styles.avatar} />
                <View style={styles.groupTitle}>
                    <Text style={styles.txtTitle}>{item.title}</Text>
                    <Text style={styles.txtDate}>{item.send_date}</Text>
                </View>
            </View>
        </View>
        <View style={styles.groupContent}>
            <Text style={styles.txtContent}>{item.content}</Text>
            <Text style={styles.txtContent}>{item.create_date}</Text>
        </View>
    </View>
);
const NotificationScreen = (
    props: BaseScreenProps<ScreenMap.Notification>,
): ReactElement => {
    const renderItem = ({item}: {item: NotifyItem}) => {
        if (item.isRead) {
            return <Item item={item} />;
        } else {
            return (
                <TouchableOpacity
                    onPress={() => {
                        props.navigation.navigate('BottomTabStack', {
                            screen: 'Notification',
                            params: {
                                screen: ScreenMap.NotificationDetail,
                                params: {item: item},
                            },
                        });
                    }}>
                    <Item item={item} />
                </TouchableOpacity>
            );
        }
    };

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.containerScroll}>
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />
            </ScrollView>
        </SafeAreaView>
    );
};
export default NotificationScreen;
