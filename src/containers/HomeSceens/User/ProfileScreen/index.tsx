import React, {ReactElement, useEffect, useState} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {BaseScreenProps} from "../../../../@types/screen-type";
import {ScreenMap} from "../../../../config/NavigationConfig";
import {useDispatch, useSelector} from "react-redux";
import {selectUserData} from "../../../../selector/AuthSelector";
import styles from "./styles";
import media from "../../../../assets/media";
import {Actions as GlobalAction} from '../../../../redux/GlobalRedux';
import {Actions as InfoAction} from '../../../../redux/InfoRedux';
import {code} from "../../../../lib/codeHelpers";
import moment from 'moment';

const ProfileScreen = (
    props: BaseScreenProps<ScreenMap.Profile>,
): ReactElement => {
    const dispatch = useDispatch();
    const userInfo = useSelector(selectUserData);
    const [helperInfo, setHelperInfor] = useState({
        birthday: 0,
        gender: 0,
        address: '',
    });

    useEffect(() => {
        const unsubcribe = props.navigation.addListener('focus', () => {
            dispatch(
                InfoAction.getInfoOwnerHelper(
                    {},
                    {
                        onBeginning: () => {
                            dispatch(GlobalAction.setShowLoading({isLoading: true}));
                        },
                        onSuccess: (response: any) => {
                            switch (response.errorCode) {
                                case code.success:
                                    const info = response.data;
                                    setHelperInfor({
                                        birthday: info.birthday,
                                        gender: info.gender,
                                        address: info.address.name,
                                    });
                                    break;
                                case code.unauthorize:
                                    props.navigation.navigate(ScreenMap.SignIn, {});
                                    break;
                            }
                        },
                        onFailure: () => {},
                        onFinish: () => {
                            dispatch(GlobalAction.setShowLoading({isLoading: true}));
                        },
                    },
                ),
            );
        });
        return unsubcribe;
    }, [dispatch, props.navigation]);


    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container}>
                <View style={styles.infoHolder}>
                    <View style={styles.groupInfoAvatar}>
                        <View style={styles.groupAvatar}>
                            <View>
                                <Image source={media.avatar_default} style={styles.avatar} />
                            </View>
                            <View style={styles.groupUserName}>
                                <Text style={styles.txtUserName}>{userInfo.fullName}</Text>
                                <Text style={styles.txtUserCode}>{userInfo.userName}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.groupUserInfo}>
                        <View style={styles.groupInfo}>
                            <Text style={styles.txtLabelInfo}>{'Ngày Sinh'}</Text>
                            <Text style={styles.txtInfo}>
                                {moment(helperInfo.birthday, 'x').format('DD/MM/YYYY')}
                            </Text>
                        </View>
                        <View style={styles.groupInfo}>
                            <Text style={styles.txtLabelInfo}>{'Giới tính'}</Text>
                            <Text style={styles.txtInfo}>
                                {helperInfo.gender == 1 ? 'Nam' : 'Nữ'}
                            </Text>
                        </View>
                        <View style={styles.groupInfo}>
                            <Text style={styles.txtLabelInfo}>{'Địa chỉ'}</Text>
                            <Text style={styles.txtInfo}>{helperInfo.address}</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>


    );

}

export default ProfileScreen;
