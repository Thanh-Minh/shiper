import React, {ReactElement, useCallback, useEffect, useState} from 'react';
import {Alert, SafeAreaView, ScrollView, Text, View, Modal} from 'react-native';
import {ScreenMap} from "../../../../config/NavigationConfig";
import {BaseScreenProps} from "../../../../@types/screen-type";
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {useDispatch} from "react-redux";
import {Actions as DataAction} from "../../../../redux/DataRedux";
import {Actions as GlobalAction} from "../../../../redux/GlobalRedux"
import {Actions as InfoAction} from "../../../../redux/InfoRedux"
import {Actions as AuthActions} from '../../../../redux/AuthRedux';
import {convert_dropdown_bank_data} from "../../../../lib/convertDataHelper";
import {code} from "../../../../lib/codeHelpers";
import DropDownPicker from "react-native-dropdown-picker";
import styles from "./styles";
import {Palette} from "../../../../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import Input from "../../../../components/Form/Input";
import AwesomeAlert from "react-native-awesome-alerts";

import TextLink from "../../../../components/Form/TextLink";

const Bankprofile = (
    props: BaseScreenProps<ScreenMap.BankProfile>
): ReactElement => {
    const dispatch = useDispatch();
    const [showAlert, setShowAlert] = useState(false);
    const [fullName, setFullName] = useState('');
    const [bankNumber, setBankNumber] = useState('');
    const [bankCode, setBankCode] = useState('');
    const [otp, setOtp] = useState('');
    const [confirmVisible, setConfirmVisible] = useState(false);
    const [listBank, setListBank] = useState([
        {
            label: 'Dữ liệu hiện tại chưa có',
            value: -1,
        },
    ]);

    useEffect(() => {
        const unsubcribe = props.navigation.addListener('focus', () => {
            dispatch(
                DataAction.getListBank(
                    {},
                    {
                        onBeginning: () => {
                        },
                        onSuccess: (response: any) => {
                            setListBank(convert_dropdown_bank_data(response.data));
                        },
                        onFailure: () => {
                        },
                        onFinish: () => {
                        },
                    },
                ),
            );
            dispatch(
                InfoAction.getUserBankInfo(
                    {},
                    {
                        onBeginning: () => {
                            dispatch(GlobalAction.setShowLoading({isLoading: true}));
                        },
                        onSuccess: (response: any) => {
                            switch (response.errorCode) {
                                case code.success:
                                    if (response.data) {
                                        setBankCode(response.data.bankCode);
                                        setBankNumber(response.data.accNum);
                                        setFullName(response.data.accName);
                                    }
                                    break;

                            }
                        },
                        onFailure: () => {
                        },
                        onFinish: () => {
                            dispatch(GlobalAction.setShowLoading({isLoading: false}));
                        },
                    },
                ),
            );
        });
        return unsubcribe;
    }, [dispatch]);
    const sendOtp = useCallback(() => {
        dispatch(
            AuthActions.sendOtp(
                {
                    typeSend: 1,
                },
                {
                    onBeginning: () => {
                    },
                    onSuccess: (response: any) => {
                        switch (response.errorCode) {

                        }
                    },
                    onFailure: (error: any) => {
                        console.warn('error', error);
                    },
                    onFinish: () => {
                    },
                },
            ),
        );
    }, []);

    const updateUserBankInfo = useCallback(
        (
            bank_code: string,
            bank_number: string,
            full_name: string,
            otp: string,
        ) => {
            if (otp.length === 4) {
                dispatch(
                    InfoAction.updateUserBankInfo(
                        {
                            bankCode: bank_code,
                            bankNumber: bank_number,
                            fullName: full_name,
                            otp: otp,
                        },
                        {
                            onBeginning: () => {
                            },
                            onSuccess: (response: any) => {
                                switch (response.errorCode) {
                                    case code.success:
                                        setShowAlert(true);
                                        break;
                                    case code.otp_code_wrong:
                                        Alert.alert('Mã OTP sai, vui lòng thử lại');
                                        break;
                                }
                            },
                            onFailure: (error: any) => {
                                console.warn('error', error);
                            },
                            onFinish: () => {
                            },
                        },
                    ),
                );
            }else { Alert.alert('Mã OTP sai, vui lòng thử lại');}
        },
        [dispatch, props.navigation],
    );

    const renderDropdown = (
        label: string,
        data: { label: string; value: number }[],
        placeHolder: string,
        zIndex: number,
    ) => {
        return (
            <>
                <Text
                    style={[styles.label, {flex: 1, marginHorizontal: 5, marginTop: 10}]}>
                    {label}
                </Text>
                <DropDownPicker
                    zIndex={zIndex}
                    items={data}
                    defaultValue={bankCode}
                    placeholder={placeHolder}
                    searchable={true}
                    searchablePlaceholder="Tìm kiếm"
                    searchablePlaceholderTextColor="gray"
                    searchableError={() => <Text>{'Không tìm thấy'}</Text>}
                    containerStyle={{height: 40, marginHorizontal: 5, marginTop: 10}}
                    style={{
                        backgroundColor: Palette.white,
                        borderWidth: 0,
                        borderBottomWidth: 0.5,
                        borderBottomColor: Palette.color_ccc,
                    }}
                    itemStyle={{
                        justifyContent: 'flex-start',
                        borderBottomWidth: 0.5,
                        borderBottomColor: Palette.color_ccc,
                    }}
                    dropDownStyle={{backgroundColor: Palette.white}}
                    placeholderStyle={{
                        color: Palette.color_ccc,
                        fontSize: RFValue(10, 580),
                    }}  onChangeList={(items, callback) => {
                    setBankCode(items.value),callback
                }}

                    selectedLabelStyle={{color: Palette.black}}
                    activeLabelStyle={{color: Palette.color_ff4}}
                    onChangeItem={item => {
                        setBankCode(item.value)
                    }}
                />
            </>
        );
    };
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container}>
                <View style={styles.bankHolder}>
                    <View style={styles.groupTitleBank}>
                        <Text style={styles.txtTitleBank}>{'Thông tin tài khoản'}</Text>
                    </View>
                    <View style={styles.groupUserInfo}>
                        {renderDropdown(
                            'Ngân hàng',
                            listBank,
                            'Vui lòng chọn Ngân hàng',
                            5000,
                        )}
                        <Input
                            label="Số tài khoản"
                            placeHolder="Nhập số tài khoản"
                            placeHolderColor={Palette.color_ccc}
                            maxLength={15}
                            marginHori={5}
                            value={bankNumber}
                            setValue={setBankNumber}
                        />
                        <Input
                            label="Chủ tài khoản"
                            placeHolder="Nhập họ và tên chủ tài khoản"
                            placeHolderColor={Palette.color_ccc}
                            maxLength={40}
                            marginHori={5}
                            value={fullName}
                            setValue={setFullName}
                        />
                    </View>
                    <AwesomeAlert
                        show={showAlert}
                        showProgress={false}
                        title="Thông báo"
                        message="Cập nhật thông tin ngân hàng thành công!"
                        closeOnTouchOutside={true}
                        closeOnHardwareBackPress={false}
                        showCancelButton={false}
                        showConfirmButton={true}
                        confirmText="Xác Nhận"
                        confirmButtonColor="#DD6B55"
                        onConfirmPressed={() => {
                            props.navigation.goBack();
                        }}
                    />
                </View>
                <View style={{marginBottom: 20}}>
                    {bankCode && bankNumber && fullName ? (
                        <ButtonSubmit
                            title="Cập nhật"
                            color={[Palette.color_ff0, Palette.color_ff4]}
                            action={() => {
                                sendOtp(), setConfirmVisible(true);
                            }}
                        />
                    ) : (
                        <ButtonSubmit
                            title="Cập nhật"
                            color={[Palette.color_c6c, Palette.color_c6c]}
                        />
                    )}
                </View>
                <Modal transparent={true} visible={confirmVisible}>
                    <View style={styles.overlay}/>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.otpTitle}>
                                {'Nhập mã OTP vừa được gửi vào số điện thoại của bạn.'}
                            </Text>
                            <OTPInputView
                                pinCount={4}
                                style={styles.otpInput}
                                codeInputFieldStyle={styles.underlineStyleBase}
                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                placeholderCharacter="-"
                                placeholderTextColor={Palette.color_1e2}
                                autoFocusOnLoad={true}
                                onCodeFilled={code => {
                                    setOtp(code);
                                }}
                            />
                            <View style={{flexDirection: 'row'}}>
                                <View style={{width: 120}}>
                                    <ButtonSubmit
                                        title="Hủy"
                                        color={[Palette.color_c6c, Palette.color_c6c]}
                                        action={() => {
                                            setConfirmVisible(false);
                                        }}
                                    />
                                </View>
                                <View style={{width: 120}}>
                                    <ButtonSubmit
                                        title="Xong"
                                        color={[Palette.color_ff0, Palette.color_ff4]}
                                        action={() => {
                                            setConfirmVisible(false),
                                                updateUserBankInfo(bankCode, bankNumber, fullName, otp);
                                        }}
                                    />
                                </View>
                            </View>
                            <TextLink
                                textTitle="Bạn không nhận được mã OTP?"
                                textLink="Gửi lại"
                                fontSize={RFValue(12, 580)}
                                color={Palette.color_e40}
                                action={() => {
                                    sendOtp();
                                }}
                            />
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        </SafeAreaView>
    );
};
export default Bankprofile;
