import React, {ReactElement, useCallback} from 'react';
import {
    Image,
    SafeAreaView,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './style';
import {BaseScreenProps} from "../../../../@types/screen-type";
import {ScreenMap} from "../../../../config/NavigationConfig";
import {selectUserData} from "../../../../selector/AuthSelector";
import {Actions as AuthActions } from "../../../../redux/AuthRedux";
import {Actions as GlobalAction} from"../../../../redux/GlobalRedux"
import media from "../../../../assets/media";
import ItemMenu from "../../../../components/Form/ItemMenu";

const MainProfileScreen = (
    props: BaseScreenProps<ScreenMap.MainProfile>,
): ReactElement => {
    const dispatch = useDispatch();
    const userInfo = useSelector(selectUserData);
    const logOut = useCallback((navigation: any) => {
        dispatch(
            AuthActions.resetAuth(
                {},
                {
                    onBeginning: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: true}));
                    },
                    onSuccess: () => {
                        navigation.navigate(ScreenMap.SignIn, {});
                    },
                    onFailure: (error: any) => {
                        console.warn('error', error);
                    },
                    onFinish: () => {
                        dispatch(GlobalAction.setShowLoading({isLoading: false}));
                    },
                },
            ),
        );
    }, []);
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container}>
                <View style={styles.infoHolder}>
                    <TouchableOpacity
                        style={styles.groupInfoAvatar}
                        onPress={() => {
                            props.navigation.navigate(ScreenMap.Profile, {});
                        }}>
                        <View style={styles.groupAvatar}>
                            <View>
                                <Image source={media.avatar_default} style={styles.avatar} />
                            </View>
                            <View style={styles.groupUserName}>
                                <Text style={styles.txtUserName}>{userInfo.fullName}</Text>
                                <Text style={styles.txtUserCode}>{userInfo.userName}</Text>
                            </View>
                        </View>
                        <View>
                            <Image
                                source={media.next}
                                style={styles.iconProfileDirect}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.menuHolder}>

                    <ItemMenu
                        label={'Ngân hàng'}
                        image={media.bank}
                        action={() => {
                            props.navigation.navigate(ScreenMap.BankProfile, {});
                        }}
                    />
                    <ItemMenu label={'Liên hệ'} image={media.support} />
                    <ItemMenu
                        label={'Đăng xuất'}
                        image={media.logout}
                        action={() => logOut(props.navigation)}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}
export default MainProfileScreen;
