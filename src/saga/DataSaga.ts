import {apiMap} from "../lib/apiMap";
import {appApi} from "../lib/fetchHelpers";
import {ActionTypes} from "../redux/DataRedux";
import { SagaIterator } from 'redux-saga';
import { takeLatest } from 'redux-saga/effects';
import { unfoldSaga, UnfoldSagaActionType } from 'redux-unfold-saga';


export function* takeGetListDistrict({
                                         payload: {},
                                         callbacks,
                                         type,
                                     }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(apiMap.listDistrict);
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeGetListWard({
                                     payload: {district_id},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(
                    apiMap.listWard + `?district_id=${district_id}`,
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeGetListBuilding({
                                     payload: {id},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(
                    apiMap.listDistrict + `/${id}`,
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeUploadImage({
                                     payload: {formData},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.post(apiMap.uploadImage, formData, {
                    headers: {
                        Authorization: 'SYSTEM_IMAGE',
                        'Content-Type': 'multipart/form-data',
                    },
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeGetListOrder({
                                     payload: {status},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                let url = apiMap.ordernew;
                status ? (url += `?status=${status} &sort=createdTime,asc`) : null;
                const {data} = await appApi.get(url);
                return data ;
            },
            key: type,
        },
        callbacks,
    );
}
export function * takePrepareOrder({
    payload:{status ,sort},
    callbacks,
    type,
                                   }:UnfoldSagaActionType):Iterable<SagaIterator>{
    yield unfoldSaga(
        {
            handler:async ():Promise<{}> =>{
                let url = apiMap.orderPrepare;
                status ? (url += `?status=${status} &sort=${sort}`) : null;
                const {data}= await appApi.get(url);
                return data
            },
            key:type
        },
        callbacks,
    );
}
export function* takeCompleteOrder({
                                       payload: {id},
                                       callbacks,
                                       type,
                                   }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.put(
                    apiMap.confirmOrder,{id: id, state: 'CONFIRM'}
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takePutCarts({
                                       payload: {id,imageUrl},
                                       callbacks,
                                       type,
                                   }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.put(
                    apiMap.putcarts,{id: id,imageUrl:imageUrl, state: 'PICKUP'}
                );

                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takePutCartsSuccess({
                                         payload: {id,imageUrl },
                                         callbacks,
                                         type,
                                     }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.put(
                    apiMap.confirmOrder,{id: id,imageUrl:imageUrl, state: 'SUCCESS'}
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeGetOrderDetail({
                                         payload: {id},
                                         callbacks,
                                         type,
                                     }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(
                    apiMap.orderPrepare + `/${id}`,
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeGetDataStatistic({
                                          payload: {startTime, endTime},
                                          callbacks,
                                          type,
                                      }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(
                    apiMap.getDataStatistic +
                    `?startTime=${startTime}&endTime=${endTime}`,
                );
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeGetListBank({
                                     payload: {},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(apiMap.getListBank);
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function * takeGetPickup({
                                       payload:{},
                                       callbacks,
                                       type,
                                   }:UnfoldSagaActionType):Iterable<SagaIterator>{
    yield unfoldSaga(
        {
            handler:async ():Promise<{}> =>{
                const {data} = await appApi.get(
                    apiMap.pickup +'?sort=createdTime,asc'

                );

                return data
            },
            key:type
        },
        callbacks,
    );
}
export default function* (): SagaIterator{
    yield takeLatest(ActionTypes.GET_DISTRICT,takeGetListDistrict);
    yield takeLatest(ActionTypes.GET_WARD,takeGetListWard);
    yield takeLatest(ActionTypes.GET_BUILDING,takeGetListBuilding);
     yield takeLatest(ActionTypes.GET_LIST_NEW_ORDER,takeGetListOrder);
    yield takeLatest(ActionTypes.UPLOAD_IMAGE, takeUploadImage);
    yield takeLatest(ActionTypes.COMPLETE_ORDER, takeCompleteOrder);
    yield takeLatest(ActionTypes.PREPARE_ORDER,takePrepareOrder);
    yield takeLatest(ActionTypes.DETAIL_ORDER,takeGetOrderDetail);
    yield takeLatest(ActionTypes.GET_DATA_STATISTIC, takeGetDataStatistic);
    yield takeLatest(ActionTypes.GET_LIST_BANK, takeGetListBank);
    yield takeLatest(ActionTypes.PUT_CARTS_SUCCESS,takePutCartsSuccess);
    yield takeLatest(ActionTypes.PUT_CARTS,takePutCarts);
    yield takeLatest(ActionTypes.GET_PICKUP,takeGetPickup)
}
