import {apiMap} from '../lib/apiMap';
import {appApi} from '../lib/fetchHelpers';
import {encryptString} from '../lib/rsaHelpers';
import {ActionTypes} from '../redux/AuthRedux';
import {Platform} from 'react-native';
import {SagaIterator} from 'redux-saga';
import {takeLatest} from 'redux-saga/effects';
import {unfoldSaga, UnfoldSagaActionType} from 'redux-unfold-saga';

export function* takeLogin({
                               payload: {phone, password},
                               callbacks,
                               type,
                           }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                let encryptPass = await encryptString(password);
                const {data} = await appApi.post(apiMap.login, {
                    device_token: '',
                    ip: '',
                    password: encryptPass,
                    source: Platform.OS.toUpperCase,
                    topicName: '',
                    type: 2,
                    username: phone,
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeSendRegisterData({
                                          payload: {phone, password},
                                          callbacks,
                                          type,
                                      }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                let encryptPass = await encryptString(password);
                const {data} = await appApi.post(apiMap.sendRegisterData, {
                    password: encryptPass,
                    type: 2,
                    username: phone,
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}


export function* takeVerifyPhone({
                                     payload: {otp},
                                     callbacks,
                                     type,
                                 }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.post(apiMap.verifyPhone, {
                    otp: otp,
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeAcceptTermOfService({
                                             payload: {},
                                             callbacks,
                                             type,
                                         }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.post(apiMap.acceptTermOfService, {});
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeResetAuth({
                                   payload: {},
                                   callbacks,
                                   type,
                               }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                return true;
            },
            key: type,
        },
        callbacks,
    );
}
export function* takeSendOtp({
                                 payload: {typeSend},
                                 callbacks,
                                 type,
                             }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.post(apiMap.sendOtp, {
                    type: typeSend,
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeInsertDeviceFireBase({
    payload: {deviceId},
    callbacks,
    type,
  }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
      {
        handler: async (): Promise<{}> => {
          const type = Platform.OS.toUpperCase();
          const {data} = await appApi.post(apiMap.insertDeviceFireBase, {
            deviceId: deviceId,
            type: type,
          });
          return data;
        },
        key: type,
      },
      callbacks,
    );
  }

export default function*(): SagaIterator {
    yield takeLatest(ActionTypes.LOGIN, takeLogin);
    yield takeLatest(ActionTypes.SEND_REGISTER_DATA, takeSendRegisterData);
    yield takeLatest(ActionTypes.VERIFY_PHONE, takeVerifyPhone);
    yield takeLatest(ActionTypes.ACCEPT_TERM_OF_SERVICE, takeAcceptTermOfService);
    yield takeLatest(ActionTypes.RESET_AUTH, takeResetAuth);
    yield takeLatest(ActionTypes.SEND_OTP, takeSendOtp);
    yield takeLatest(
        ActionTypes.INSERT_DEVICE_FIRE_BASE,
        takeInsertDeviceFireBase,
      );
}
