import {apiMap} from "../lib/apiMap";
import {appApi} from "../lib/fetchHelpers";
import {ActionTypes} from "../redux/InfoRedux";
import {SagaIterator} from "redux-saga";
import {takeLatest} from 'redux-saga/effects';
import {unfoldSaga, UnfoldSagaActionType} from "redux-unfold-saga";

export function* takeUpdateOwnerHelperInfo(
    {
        payload: {address, birthday, building_id,listImage,full_name, gender, ward_id},
        callbacks,
        type,
    }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async ():Promise<{}>=>{
                const {data} = await appApi.post(apiMap.updateOwnerHelperInfo,{
                    address: address,
                    birthday: birthday,
                    building_id: building_id,
                    cert_images: listImage,
                    full_name: full_name,
                    gender: gender,
                    ward_id: ward_id
                });
                return data;
            },
            key:type,
        },
        callbacks,
    );
}
export function* takeUpdateUserInfoBank({
                                            payload: {bankCode, bankNumber, fullName, otp},
                                            callbacks,
                                            type,
                                        }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.put(apiMap.updateUserBankInfo, {
                    bank_code: bankCode,
                    bank_number: bankNumber,
                    full_name: fullName,
                    otp: otp,
                });
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeUserInfoBank({
                                      payload: {},
                                      callbacks,
                                      type,
                                  }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(apiMap.updateUserBankInfo);
                return data;
            },
            key: type,
        },
        callbacks,
    );
}

export function* takeGetInfoOwnerHelper({
                                      payload: {},
                                      callbacks,
                                      type,
                                  }: UnfoldSagaActionType): Iterable<SagaIterator> {
    yield unfoldSaga(
        {
            handler: async (): Promise<{}> => {
                const {data} = await appApi.get(apiMap.getOwnerHelperInfo);
                return data;
            },
            key: type,
        },
        callbacks,
    );
}
export default function*(): SagaIterator{
    yield takeLatest(ActionTypes.UPDATE_OWNER_HELPER_INFO,takeUpdateOwnerHelperInfo);
    yield takeLatest(ActionTypes.UPDATE_USER_BANK_INFO, takeUpdateUserInfoBank);
    yield takeLatest(ActionTypes.GET_USER_BANK_INFO, takeUserInfoBank);
    yield  takeLatest(ActionTypes.GET_INFO_OWNER_HELPER,takeGetInfoOwnerHelper);
}
