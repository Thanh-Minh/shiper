import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {ScreenMap, ScreenParams} from "../config/NavigationConfig";
import MainProfileScreen from "../containers/HomeSceens/User/MainProfileScreen";
import ImageHeader from "../components/ImageHeader";
import {Palette} from "../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";
import ProfileScreen from "../containers/HomeSceens/User/ProfileScreen";
import BankProfileScreen from "../containers/HomeSceens/User/BankProfileScreen";

const Stack= createStackNavigator<ScreenParams>();

const UserStack = () =>{
    return(
        <Stack.Navigator initialRouteName={ScreenMap.MainProfile}>
            <Stack.Screen name={ScreenMap.MainProfile}
            component={MainProfileScreen}
                          options={{
                              headerTitle:'Tài khoản ',
                              headerLeft:()=>{
                                  return null;
                              },
                              headerBackground:()=> <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}
            />
            <Stack.Screen
                name={ScreenMap.Profile}
                component={ProfileScreen}
                options={{
                    headerTitle: 'Thông tin tài khoản',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
            <Stack.Screen
                name={ScreenMap.BankProfile}
                component={BankProfileScreen}
                options={{
                    headerTitle: 'Thông tin ngân hàng',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    )
}
export default UserStack;
