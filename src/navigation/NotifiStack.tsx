import {ScreenMap} from "../config/NavigationConfig";
import {Image} from "react-native";
import media from "../assets/media";
import ImageHeader from "../components/ImageHeader";
import {Palette} from "../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import NotificationScreen from "../containers/HomeSceens/Notification";
import NotifiDetailScreen from "../containers/HomeSceens/Notification/NotifiDetailScreen";


const Stack= createStackNavigator();
const NotifiStack = () => {
    return (
        <Stack.Navigator initialRouteName={ScreenMap.Order}>
            <Stack.Screen
                name={ScreenMap.Order}
                component={NotificationScreen}
                options={{
                    headerTitle: 'Thông báo',
                    headerLeft: () => {
                        return null;
                    },
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
            <Stack.Screen
                name={ScreenMap.StatisticsDetail}
                component={NotifiDetailScreen}
                options={{
                    headerTitle: 'Thông báo',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    );
};
export default NotifiStack;
