import React, {} from 'react';
import { Image} from 'react-native';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import media from "../assets/media";
import {Palette} from "../theme/Palette";
import Pickup from "../containers/HomeSceens/Pickup";
import {createStackNavigator} from "@react-navigation/stack";
import ImageHeader from "../components/ImageHeader";
import {RFValue} from "react-native-responsive-fontsize";
import OrderStack from "./OrderStack";
import {ScreenMap} from "../config/NavigationConfig";
import StatisticsStack from "./StatisticsStack";
import NotificationStack from "./NotificationStack";
import {getBottomSpace} from "react-native-iphone-x-helper";
import UserStack from './UserStack';

const Stack= createStackNavigator();

function PickupStack  (){
    return(
        <Stack.Navigator>
            <Stack.Screen name='Pickup'
                          component={Pickup}
                          options={{
                              headerTitle: 'Lấy hàng',
                              headerLeft: () => {
                                  return null;
                              },
                              headerBackground: () => <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}/>
        </Stack.Navigator>
    )
}



const Tab = createBottomTabNavigator();
const BottomTabStack = () => {
    const getTabBarVisibility = (route: any) => {
        const routeName = route.state
            ? route.state.routes[route.state.index].name
            : '';

        if (
            routeName === ScreenMap.OrderDetail ||
            routeName === ScreenMap.StatisticsDetail ||
            routeName === ScreenMap.NotificationDetail ||
            routeName === ScreenMap.BankProfile ||
            routeName === ScreenMap.Profile
        ) {
            return false;
        }

        return true;
    };
        return(
         <Tab.Navigator

             tabBarOptions={{
                 activeTintColor: Palette.color_e91,
                 labelStyle: {
                     fontSize: 12,
                 },
                 style: {height: 50,
                     paddingBottom: 0,
                     marginBottom: getBottomSpace(), },
             }}
             >
             <Tab.Screen name='Order'
                         component={OrderStack}
                         options={({route}) => ({
                             tabBarLabel: 'Đơn hàng',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.order_click : media.order}
                                     style={{width: 22, height: 22, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}
             />
             <Tab.Screen name='Pickup'
                         component={PickupStack}
                         options={({route}) => ({
                             tabBarLabel: 'Lấy hàng',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.pickup_click : media.pickup}
                                     style={{width:22, height: 22, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='Statistics'
                         component={StatisticsStack}
                         options={({route}) => ({
                             tabBarLabel: 'Thống kê',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.statistics_click : media.statistics}
                                     style={{width: 22, height: 22, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='Notification'
                         component={NotificationStack}
                         options={({route}) => ({
                             tabBarLabel: 'Thông báo',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.notification_click : media.notification}
                                     style={{width: 22, height: 22, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='User'
                         component={UserStack}
                         options={({route}) => ({
                             tabBarLabel: 'Tài khoản',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.account_click : media.account}
                                     style={{width: 22, height: 22, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>


         </Tab.Navigator>
     )
}
export default BottomTabStack;
