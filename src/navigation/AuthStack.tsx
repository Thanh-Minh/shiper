import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {ScreenMap, ScreenParams} from "../config/NavigationConfig";
import SignInScreen from "../containers/auth/SigninScreen";
import SignUpScreen from "../containers/auth/SignupScreen";
import OtpScreen from "../containers/auth/OtpScreen";
import ProvisionScreen from "../containers/auth/ProvisionScreen";
import ImageHeader from "../components/ImageHeader";
import {Palette} from "../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";
import UpdateProfileInfo from "../containers/auth/UpdateProfileInfo";

const Stack = createStackNavigator<ScreenParams>();

const AuthStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name={ScreenMap.SignIn}
                component={SignInScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={ScreenMap.SignUp}
                component={SignUpScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name={ScreenMap.Otp}
                component={OtpScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name={ScreenMap.Provision}
                component={ProvisionScreen}
                options={{
                    headerTitle: 'Điều khoản sử dụng',
                    headerLeft: () => {
                        return null
                    },
                    headerBackground: () => <ImageHeader/>,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center'
                }}
            />
            <Stack.Screen
                name={ScreenMap.UpdateOwnerHelperInfo}
                component={UpdateProfileInfo}
                options={{
                    headerTitle: 'Cập nhật thông tin của bạn',
                    headerLeft: () => {
                        return null
                    },
                    // headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader/>,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    )
}

export default AuthStack;
