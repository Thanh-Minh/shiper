import {createStackNavigator} from "@react-navigation/stack";
import ImageHeader from "../components/ImageHeader";
import {ScreenMap, ScreenParams} from "../config/NavigationConfig";
import Order from "../containers/HomeSceens/Order";
import {Palette} from "../theme/Palette";
import React from "react";
import {RFValue} from "react-native-responsive-fontsize";
import DeatailOrderScreen from "../containers/HomeSceens/Order/Detail";
import TakingOrder from "../containers/HomeSceens/Order/Orderprepare";


const Stack = createStackNavigator<ScreenParams>();

const OrderStack = () => {
    return (
        <Stack.Navigator initialRouteName={ScreenMap.Order}>
            <Stack.Screen
                name={ScreenMap.Order}
                component={Order}
                options={{
                    headerTitle: 'Nhận đơn',
                    headerLeft: () => {
                        return null;
                    },
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
            {/*<Stack.Screen name={ScreenMap.Orderprepare}*/}
            {/*              component={Orderprepare}*/}
            {/*/>*/}
            <Stack.Screen
                name={ScreenMap.OrderDetail}
                component={DeatailOrderScreen}
                options={{
                    headerTitle: 'Chi tiết đơn hàng',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    );
};
export default OrderStack;
