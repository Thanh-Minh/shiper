import {createStackNavigator} from "@react-navigation/stack";
import Account from "../containers/HomeSceens/User";
import ProfileScreen from "../containers/HomeSceens/User/ProfileScreen";
import {Palette} from "../theme/Palette";
import ImageHeader from "../components/ImageHeader";
import {RFValue} from "react-native-responsive-fontsize";
import React from "react";
import {ScreenMap} from "../config/NavigationConfig";
import BankingScreen from "../containers/HomeSceens/User/BankProfileScreen";


const Stack= createStackNavigator();
const AccountStack =()=>{
    return(
        <Stack.Navigator initialRouteName={ScreenMap.Account}>
            <Stack.Screen name={ScreenMap.Account}
                          component={Account}
                          options={{
                              headerTitle: 'Tài khoản',
                              headerLeft: () => {
                                  return null;
                              },
                              headerBackground: () => <ImageHeader />,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}
                          />
                          <Stack.Screen name={ScreenMap.Profile}
                          component={ProfileScreen}
                                        options={{
                                            headerTitle: 'Thông tin tài khoản',

                                            headerBackground: () => <ImageHeader />,
                                            headerTintColor: Palette.white,
                                            headerTitleStyle: {fontSize: RFValue(15, 580)},
                                            headerTitleAlign: 'center',
                                        }}
                          />
                          <Stack.Screen name={ScreenMap.BankProfile}
                                        component={BankingScreen}
                                        options={{
                                            headerTitle: 'Thông tin tài khoản',

                                            headerBackground: () => <ImageHeader />,
                                            headerTintColor: Palette.white,
                                            headerTitleStyle: {fontSize: RFValue(15, 580)},
                                            headerTitleAlign: 'center',
                                        }}
                                        />
        </Stack.Navigator>
    )
}
export default AccountStack
