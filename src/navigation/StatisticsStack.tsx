import {ScreenMap} from "../config/NavigationConfig";
import {Image} from "react-native";
import media from "../assets/media";
import ImageHeader from "../components/ImageHeader";
import {Palette} from "../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import StatisticScreen from "../containers/HomeSceens/Statistics";
import StatisticsDetail from "../containers/HomeSceens/Statistics/StatisticsDetail";

const Stack= createStackNavigator();
const StatisticsStack = () => {
    return (
        <Stack.Navigator initialRouteName={ScreenMap.Statistics}>
            <Stack.Screen
                name={ScreenMap.Statistics}
                component={StatisticScreen}
                options={{
                    headerTitle: 'Thống kê',
                    headerLeft: () => {
                        return null;
                    },
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
            <Stack.Screen
                name={ScreenMap.StatisticsDetail}
                component={StatisticsDetail}
                options={{
                    headerTitle: 'Chi tiết đánh giá',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    );
};
export default StatisticsStack;
