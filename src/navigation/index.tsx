import React, { useCallback, useEffect } from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {ScreenParams} from "../config/NavigationConfig";
import {SafeAreaProvider} from "react-native-safe-area-context";
import AuthStack from "./AuthStack";
import BottomTabStack from "./BottomTabStack";
import { useDispatch } from 'react-redux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';
import {Actions as AuthActions} from '../redux/AuthRedux';
import {Actions as GlobalAction} from '../redux/GlobalRedux';
import { code } from '../lib/codeHelpers';

const Stack = createStackNavigator<ScreenParams>();
const Navigation = () => {
    const dispatch = useDispatch();

  const checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      getToken();
    } else {
      requestPermission();
    }
  };
  const getToken = async () => {
    let fcmToken = await firebase.messaging().getToken();
    console.log(fcmToken);
    if (fcmToken) {
      await AsyncStorage.setItem('fcmToken', fcmToken);
    } else {
      requestPermission();
    }
  };

  const requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      getToken();
    } catch (error) {}
  };

  useEffect(() => {
    checkPermission();
    createNotificationListeners();
    handleNotification();
  }, []);

  const handleNotification = async () => {
    firebase.notifications().onNotificationOpened(async notificationOpen => {
      const token = await AsyncStorage.getItem('access_token');
      const notification = notificationOpen.notification;
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      if (token) {
        if (notification.data.type == '0') {
          const arr: [] = [];
          // updateStatusNotify(fcmToken, arr, 1, 3, 1);
          // navigation.navigate('BottomTab', {screen: 'Notify'});
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
          firebase.notifications().removeAllDeliveredNotifications();
        } else {
          const arr = [];
          arr.push(notification.data.idNoti);
          // updateStatusNotify(fcmToken, arr, 1, 2, 2);
          setTimeout(() => {
            // navigation.navigate('BottomTab', {screen: 'Notify'});
            firebase
              .notifications()
              .removeDeliveredNotification(notification.notificationId);
            firebase.notifications().removeAllDeliveredNotifications();
          }, 200);
        }
      }
    });
    await firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          const notification = notificationOpen.notification;
          setTimeout(() => {
            if (notification.data) {
              Alert.alert('Thông báo', notification.data.message);
            } else {
              Alert.alert('Thông báo', notification.body);
            }
          }, 1000);
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
        }
      });
  };

  const createNotificationListeners = async () => {
    firebase.messaging().onTokenRefresh(async () => {
      let newfcmToken = await firebase.messaging().getToken();
      if (newfcmToken) {
        await AsyncStorage.setItem('fcmToken', newfcmToken);
        insertDevice(newfcmToken);
      } else {
        requestPermission();
        insertDevice(newfcmToken);
      }
    });
    firebase.notifications().onNotification(notification => {
      firebase.notifications().displayNotification(notification);
      // setTimeout(() => {
      // if (token) {
      // getDataNotification();
      // }
      // }, 50);
    });
    firebase.messaging().onMessage(message => {
      console.log(message);
      // setTimeout(() => {
      // if (token) {
      // getDataNotification();
      // }
      // }, 50);
    });
  };
  const insertDevice = useCallback((newfcmToken: string) => {
    dispatch(
      AuthActions.insertDeviceFireBase(
        {
          deviceId: newfcmToken,
        },
        {
          onBeginning: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: true}));
          },
          onSuccess: (response: any) => {
            switch (response.errorCode) {
              case code.success:
                console.log('Insert deviceId Success');
                break;
            }
          },
          onFailure: (error: any) => {
            console.warn('error', error);
          },
          onFinish: () => {
            dispatch(GlobalAction.setShowLoading({isLoading: false}));
          },
        },
      ),
    );
  }, []);
    return (
        <SafeAreaProvider>
            <NavigationContainer>
                <Stack.Navigator initialRouteName='Auth'>
                    <Stack.Screen name="Auth"
                                  component={AuthStack}
                                  options={{headerShown: false}}/>
                    <Stack.Screen name='BottomTabStack'
                                  component={BottomTabStack}
                                  options={{headerShown: false}}
                    />

                </Stack.Navigator>
            </NavigationContainer>
        </SafeAreaProvider>
    )
}
export default Navigation;
